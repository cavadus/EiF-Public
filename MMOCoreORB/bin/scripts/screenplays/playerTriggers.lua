PlayerTriggers = { }

function PlayerTriggers:playerLoggedIn(pPlayer)
	if (pPlayer == nil) then
		return
	end
	ServerEventAutomation:playerLoggedIn(pPlayer)
	BestineElection:playerLoggedIn(pPlayer)
	--createEvent(15 * 1000, "overquestScreenplay", "newPlayerStart", pPlayer, "")
	createEvent(30 * 1000, "boontaEveScreenplay", "scheduleHolocomm", pPlayer, "")
	createEvent(30 * 1000, "eggHuntScreenplay", "scheduleHolocomm", pPlayer, "")
	createEvent(30 * 1000, "empireDayScreenplay", "scheduleHolocomm", pPlayer, "")
	createEvent(getRandomNumber(5, 30) * 60 * 1000, "QuixoteQuestScreenplay", "spawnOldMan", pPlayer, "")
	createEvent(30 * 1000, "riddleHuntScreenplay", "scheduleHolocomm", pPlayer, "")
	createEvent(90 * 1000, "HolocommScreenplay", "callPlayer", pPlayer, "")
	createEvent(5 * 1000, "JediUnlockScreenplay", "forceCheck", pPlayer, "")
	JediTrainingScreenplay:observerSetup(pPlayer)
	questContentScreenplay:refreshObservers(pPlayer)
	JediEncountersScreenplay:loginCheck(pPlayer)
end

function PlayerTriggers:playerLoggedOut(pPlayer)
	if (pPlayer == nil) then
		return
	end
	ServerEventAutomation:playerLoggedOut(pPlayer)
	EmpireDayEndorPvPScreenplay:removePlayerFromQueue(pPlayer)
end
