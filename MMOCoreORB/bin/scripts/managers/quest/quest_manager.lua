local ObjectManager = require("managers.object.object_manager")

local QuestManager = {}

local QUEST_ACTIVE = 1
local QUEST_COMPLETED = 1

-- Activate the quest for the player.
-- @param pCreatureObject pointer to the creature object of the player.
-- @param quest the index number for the quest to activate.
function QuestManager.activateQuest(pCreatureObject, quest)
	local pGhost = CreatureObject(pCreatureObject):getPlayerObject()

	if (pGhost == nil) then
		return
	end

	if (QuestManager.shouldSendSystemMessage(pCreatureObject, quest)) then
		local summary = getStringId(QuestManager.getJournalSummary(quest))

		if (summary ~= "") then
			CreatureObject(pCreatureObject):sendSystemMessage("Quest Started: \\#FFFF33\\" .. summary)
		end

		CreatureObject(pCreatureObject):sendSystemMessage("@quest/quests:quest_journal_updated")
	end

	PlayerObject(pGhost):setActiveQuestsBit(quest, QUEST_ACTIVE)
end

function QuestManager.activateQuestSilent(pCreatureObject, quest)
	local pGhost = CreatureObject(pCreatureObject):getPlayerObject()

	if (pGhost == nil) then
		return
	end

	PlayerObject(pGhost):setActiveQuestsBit(quest, QUEST_ACTIVE)
end

-- Checks if the player has a quest active.
-- @param pCreatureObject pointer to the creature object of the player.
-- @param quest the index number for the quest to check if it is active.
function QuestManager.hasActiveQuest(pCreatureObject, quest)
	local pGhost = CreatureObject(pCreatureObject):getPlayerObject()

	if (pGhost == nil) then
		return false
	end

	return PlayerObject(pGhost):hasActiveQuestBitSet(quest)
end

-- Complete the quest for the player.
-- @param pCreatureObject pointer to the creature object of the player.
-- @param quest the index number for the quest to complete.
function QuestManager.completeQuest(pCreatureObject, quest)
	local pGhost = CreatureObject(pCreatureObject):getPlayerObject()

	if (pGhost == nil) then
		return
	end

	if (QuestManager.shouldSendSystemMessage(pCreatureObject, quest)) then
		local summary = getStringId(QuestManager.getJournalSummary(quest))

		if (summary ~= "") then
			CreatureObject(pCreatureObject):sendSystemMessage("Quest Completed: \\#FFFF33\\" .. summary)
		end
	end

	PlayerObject(pGhost):clearActiveQuestsBit(quest)
	PlayerObject(pGhost):setCompletedQuestsBit(quest, QUEST_COMPLETED)
end


function QuestManager.completeQuestSilent(pCreatureObject, quest)
	local pGhost = CreatureObject(pCreatureObject):getPlayerObject()

	if (pGhost == nil) then
		return
	end

	PlayerObject(pGhost):clearActiveQuestsBit(quest)
	PlayerObject(pGhost):setCompletedQuestsBit(quest, QUEST_COMPLETED)
end

-- Un-Complete the quest for the player and set quest active again.
-- @param pCreatureObject pointer to the creature object of the player.
-- @param quest the index number for the quest to complete.
function QuestManager.uncompleteQuest(pCreatureObject, quest)
	local pGhost = CreatureObject(pCreatureObject):getPlayerObject()

	if (pGhost == nil) then
		return
	end

	PlayerObject(pGhost):clearCompletedQuestsBit(quest)
	PlayerObject(pGhost):setActiveQuestsBit(quest, QUEST_ACTIVE)
end

-- Checks if the player has a quest completed.
-- @param pCreatureObject pointer to the creature object of the player.
-- @param quest the index number for the quest to check if it is completed.
function QuestManager.hasCompletedQuest(pCreatureObject, quest)
	local pGhost = CreatureObject(pCreatureObject):getPlayerObject()

	if (pGhost == nil) then
		return false
	end

	return PlayerObject(pGhost):hasCompletedQuestsBitSet(quest)
end

-- Reset the quest for the player.
-- @param pCreatureObject pointer to the creature object of the player.
-- @param quest the index number for the quest to reset.
function QuestManager.resetQuest(pCreatureObject, quest)
	local pGhost = CreatureObject(pCreatureObject):getPlayerObject()

	if (pGhost == nil) then
		return
	end

	PlayerObject(pGhost):clearActiveQuestsBit(quest)
	PlayerObject(pGhost):clearCompletedQuestsBit(quest)
end

function QuestManager.failQuest(pCreatureObject, quest)
	if (QuestManager.shouldSendSystemMessage(pCreatureObject, quest)) then
		local summary = getStringId(QuestManager.getJournalSummary(quest))

		if (summary ~= "") then
			CreatureObject(pCreatureObject):sendSystemMessage("Quest Failed: \\#FFFF33\\" .. summary)
		end
	end

	QuestManager.resetQuest(pCreatureObject, quest)
end

function QuestManager.failQuestSilent(pCreatureObject, quest)
	QuestManager.resetQuest(pCreatureObject, quest)
end

function QuestManager.shouldSendSystemMessage(pCreatureObject, quest)
	local pQuest = getQuestInfo(quest)

	if (pQuest == nil) then
		return false
	end

	return LuaQuestInfo(pQuest):shouldSendSystemMessage()
end

function QuestManager.getQuestName(questID)
	local pQuest = getQuestInfo(questID)

	if (pQuest == nil) then
		return ""
	end

	return LuaQuestInfo(pQuest):getQuestName()
end

function QuestManager.getJournalSummary(questID)
	local pQuest = getQuestInfo(questID)

	if (pQuest == nil) then
		return ""
	end

	return LuaQuestInfo(pQuest):getJournalSummary()
end

function QuestManager.getCurrentQuestID(pPlayer)
	local id = tonumber(readScreenPlayData(pPlayer, "VillageJediProgression", "CurrentQuestID"))

	if (id == nil) then
		id = -1
	end

	return id
end

function QuestManager.setCurrentQuestID(pPlayer, qid)
	return writeScreenPlayData(pPlayer, "VillageJediProgression", "CurrentQuestID", qid)
end

function QuestManager.getStoredVillageValue(pPlayer, key)
	return readScreenPlayData(pPlayer, "VillageJediProgression", key)
end

function QuestManager.setStoredVillageValue(pPlayer, key, value)
	return writeScreenPlayData(pPlayer, "VillageJediProgression", key, value)
end

QuestManager.quests = {}

QuestManager.quests.TEST_SIMPLE 				= 0
QuestManager.quests.TEST_GOTO_01 				= 1
QuestManager.quests.TEST_GOTO_02 				= 2
QuestManager.quests.TEST_GOTO_03 				= 3
QuestManager.quests.TEST_FIND_01 				= 4
QuestManager.quests.TEST_ENCOUNTER_01 				= 5
QuestManager.quests.TEST_DESTROY_01 				= 6
QuestManager.quests.TEST_ESCORT_01 				= 7
QuestManager.quests.TEST_ESCORT_LOCATION 			= 8
QuestManager.quests.TEST_RANDOM 				= 9
QuestManager.quests.TEST_GIVE 					= 10
QuestManager.quests.SCT1 					= 11
QuestManager.quests.SCT2 					= 12
QuestManager.quests.SCT3 					= 13
QuestManager.quests.SCT4 					= 14
QuestManager.quests.FS_QUESTS_SAD_TASKS 			= 15
QuestManager.quests.FS_QUESTS_SAD_TASK1 			= 16
QuestManager.quests.FS_QUESTS_SAD_RETURN1 			= 17
QuestManager.quests.FS_QUESTS_SAD_TASK2 			= 18
QuestManager.quests.FS_QUESTS_SAD_RETURN2 			= 19
QuestManager.quests.FS_QUESTS_SAD_TASK3 			= 20
QuestManager.quests.FS_QUESTS_SAD_RETURN3 			= 21
QuestManager.quests.FS_QUESTS_SAD_TASK4 			= 22
QuestManager.quests.FS_QUESTS_SAD_RETURN4 			= 23
QuestManager.quests.FS_QUESTS_SAD_TASK5 			= 24
QuestManager.quests.FS_QUESTS_SAD_RETURN5 			= 25
QuestManager.quests.FS_QUESTS_SAD_TASK6 			= 26
QuestManager.quests.FS_QUESTS_SAD_RETURN6 			= 27
QuestManager.quests.FS_QUESTS_SAD_TASK7 			= 28
QuestManager.quests.FS_QUESTS_SAD_RETURN7 			= 29
QuestManager.quests.FS_QUESTS_SAD_TASK8 			= 30
QuestManager.quests.FS_QUESTS_SAD_RETURN8 			= 31
QuestManager.quests.FS_QUESTS_SAD_FINISH 			= 32
QuestManager.quests.FS_QUESTS_SAD2_TASKS 			= 33
QuestManager.quests.FS_QUESTS_SAD2_TASK1			= 34
QuestManager.quests.FS_QUESTS_SAD2_RETURN1 			= 35
QuestManager.quests.FS_QUESTS_SAD2_TASK2 			= 36
QuestManager.quests.FS_QUESTS_SAD2_RETURN2 			= 37
QuestManager.quests.FS_QUESTS_SAD2_TASK3 			= 38
QuestManager.quests.FS_QUESTS_SAD2_RETURN3 			= 39
QuestManager.quests.FS_QUESTS_SAD2_TASK4 			= 40
QuestManager.quests.FS_QUESTS_SAD2_RETURN4 			= 41
QuestManager.quests.FS_QUESTS_SAD2_TASK5 			= 42
QuestManager.quests.FS_QUESTS_SAD2_RETURN5 			= 43
QuestManager.quests.FS_QUESTS_SAD2_TASK6 			= 44
QuestManager.quests.FS_QUESTS_SAD2_RETURN6 			= 45
QuestManager.quests.FS_QUESTS_SAD2_TASK7 			= 46
QuestManager.quests.FS_QUESTS_SAD2_RETURN7 			= 47
QuestManager.quests.FS_QUESTS_SAD2_TASK8 			= 48
QuestManager.quests.FS_QUESTS_SAD2_RETURN8 			= 49
QuestManager.quests.FS_QUESTS_SAD2_FINISH 			= 50
QuestManager.quests.FS_MEDIC_PUZZLE_QUEST_01 			= 51
QuestManager.quests.FS_MEDIC_PUZZLE_QUEST_02 			= 52
QuestManager.quests.FS_MEDIC_PUZZLE_QUEST_03 			= 53
QuestManager.quests.FS_PHASE_2_CRAFT_DEFENSES_02	 	= 54
QuestManager.quests.FS_PHASE_3_CRAFT_SHIELDS_02 		= 55
QuestManager.quests.FS_REFLEX_RESCUE_QUEST_00 			= 56
QuestManager.quests.FS_REFLEX_RESCUE_QUEST_01 			= 57
QuestManager.quests.FS_REFLEX_RESCUE_QUEST_02 			= 58
QuestManager.quests.FS_REFLEX_RESCUE_QUEST_03 			= 59
QuestManager.quests.FS_REFLEX_RESCUE_QUEST_04 			= 60
QuestManager.quests.FS_REFLEX_RESCUE_QUEST_05 			= 61
QuestManager.quests.FS_REFLEX_RESCUE_QUEST_06 			= 62
QuestManager.quests.FS_REFLEX_FETCH_QUEST_00 			= 63
QuestManager.quests.FS_REFLEX_FETCH_QUEST_01 			= 64
QuestManager.quests.FS_REFLEX_FETCH_QUEST_02 			= 65
QuestManager.quests.FS_REFLEX_FETCH_QUEST_03 			= 66
QuestManager.quests.FS_REFLEX_FETCH_QUEST_04 			= 67
QuestManager.quests.FS_REFLEX_FETCH_QUEST_05 			= 68
QuestManager.quests.FS_REFLEX_FETCH_QUEST_06 			= 69
QuestManager.quests.FS_CRAFT_PUZZLE_QUEST_00 			= 70
QuestManager.quests.FS_CRAFT_PUZZLE_QUEST_01 			= 71
QuestManager.quests.FS_CRAFT_PUZZLE_QUEST_02 			= 72
QuestManager.quests.FS_CRAFT_PUZZLE_QUEST_03 			= 73
QuestManager.quests.OLD_MAN_INITIAL 				= 74
QuestManager.quests.FS_THEATER_CAMP 				= 75
QuestManager.quests.DO_NOT_USE_BAD_SLOT 			= 76
QuestManager.quests.FS_GOTO_DATH 				= 77
QuestManager.quests.FS_VILLAGE_ELDER 				= 78
QuestManager.quests.OLD_MAN_FORCE_CRYSTAL 			= 79
QuestManager.quests.FS_DATH_WOMAN 				= 80
QuestManager.quests.FS_PATROL_QUEST_1 				= 81
QuestManager.quests.FS_PATROL_QUEST_2 				= 82
QuestManager.quests.FS_PATROL_QUEST_3 				= 83
QuestManager.quests.FS_PATROL_QUEST_4 				= 84
QuestManager.quests.FS_PATROL_QUEST_5 				= 85
QuestManager.quests.FS_PATROL_QUEST_6 				= 86
QuestManager.quests.FS_PATROL_QUEST_7 				= 87
QuestManager.quests.FS_PATROL_QUEST_8 				= 88
QuestManager.quests.FS_PATROL_QUEST_9 				= 89
QuestManager.quests.FS_PATROL_QUEST_10 				= 90
QuestManager.quests.FS_PATROL_QUEST_11 				= 91
QuestManager.quests.FS_PATROL_QUEST_12 				= 92
QuestManager.quests.FS_PATROL_QUEST_13 				= 93
QuestManager.quests.FS_PATROL_QUEST_14 				= 94
QuestManager.quests.FS_PATROL_QUEST_15 				= 95
QuestManager.quests.FS_PATROL_QUEST_16 				= 96
QuestManager.quests.FS_PATROL_QUEST_17 				= 97
QuestManager.quests.FS_PATROL_QUEST_18 				= 98
QuestManager.quests.FS_PATROL_QUEST_19 				= 99
QuestManager.quests.FS_PATROL_QUEST_20 				= 100
QuestManager.quests.FS_COMBAT_HEALING_1 			= 101
QuestManager.quests.FS_COMBAT_HEALING_2 			= 102
QuestManager.quests.FS_DEFEND_SET_FACTION 			= 103
QuestManager.quests.FS_DEFEND_01 				= 104
QuestManager.quests.FS_DEFEND_02 				= 105
QuestManager.quests.FS_DEFEND_REwARD_01 			= 106
QuestManager.quests.FS_DEFEND_03 				= 107
QuestManager.quests.FS_DEFEND_04 				= 108
QuestManager.quests.FS_CS_INTRO 				= 109
QuestManager.quests.FS_CS_KILL5_GUARDS 				= 110
QuestManager.quests.FS_CS_ENSURE_CAPTURE 			= 111
QuestManager.quests.FS_CS_LAST_CHANCE 				= 112
QuestManager.quests.FS_CS_ESCORT_COMMANDER_PRI 			= 113
QuestManager.quests.FS_CS_ESCORT_COMMANDER_SEC 			= 114
QuestManager.quests.FS_CS_QUEST_DONE 				= 115
QuestManager.quests.FS_THEATER_FINAL 				= 116
QuestManager.quests.OLD_MAN_FINAL 				= 117
QuestManager.quests.FS_CRAFTING4_QUEST_00 			= 118
QuestManager.quests.FS_CRAFTING4_QUEST_01 			= 119
QuestManager.quests.FS_CRAFTING4_QUEST_02 			= 120
QuestManager.quests.FS_CRAFTING4_QUEST_03 			= 121
QuestManager.quests.FS_CRAFTING4_QUEST_04 			= 122
QuestManager.quests.FS_CRAFTING4_QUEST_05 			= 123
QuestManager.quests.FS_CRAFTING4_QUEST_06 			= 124
QuestManager.quests.FS_CRAFTING4_QUEST_07 			= 125
QuestManager.quests.TWO_MILITARY 				= 126
QuestManager.quests.FS_DEFEND_REwARD_02 			= 127
QuestManager.quests.FS_DEFEND_REwARD_03 			= 128
QuestManager.quests.SURVEY_PHASE2_MAIN 				= 129
QuestManager.quests.SURVEY_PHASE2_01 				= 130
QuestManager.quests.SURVEY_PHASE2_02 				= 131
QuestManager.quests.SURVEY_PHASE2_03 				= 132
QuestManager.quests.SURVEY_PHASE2_04 				= 133
QuestManager.quests.SURVEY_PHASE2_05 				= 134
QuestManager.quests.SURVEY_PHASE2_06 				= 135
QuestManager.quests.SURVEY_PHASE2_07 				= 136
QuestManager.quests.SURVEY_PHASE2_08 				= 137
QuestManager.quests.SURVEY_PHASE3_MAIN 				= 138
QuestManager.quests.SURVEY_PHASE3_01 				= 139
QuestManager.quests.SURVEY_PHASE3_02 				= 140
QuestManager.quests.SURVEY_PHASE3_03 				= 141
QuestManager.quests.SURVEY_PHASE3_04 				= 142
QuestManager.quests.SURVEY_PHASE3_05 				= 143
QuestManager.quests.SURVEY_PHASE3_06 				= 144
QuestManager.quests.SURVEY_PHASE3_07 				= 145
QuestManager.quests.SURVEY_PHASE3_08 				= 146
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_01 	= 147
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_02 	= 148
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_03 	= 149
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_04 	= 150
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_05 	= 151
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_06 	= 152
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_07 	= 153
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_08 	= 154
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_09 	= 155
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_10 	= 156
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_11 	= 157
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_12 	= 158
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_13 	= 159
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_14 	= 160
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_15 	= 161
QuestManager.quests.FS_SURVEY_SPECIAL_RESOURCE_16 	= 162
QuestManager.quests.FS_DATH_wOMAN_TALK 			= 163
QuestManager.quests.FS_PATROL_QUEST_START 		= 164
QuestManager.quests.FS_REFLEX_RESCUE_QUEST_07 		= 165
QuestManager.quests.FS_SURVEY_PHASE2_REwARD 		= 166
QuestManager.quests.FS_SURVEY_PHASE3_REwARD 		= 167
QuestManager.quests.FS_DEFEND_SET_FACTION_02 		= 168
QuestManager.quests.LOOT_DATAPAD_1 			= 169
QuestManager.quests.GOT_DATAPAD 			= 170
QuestManager.quests.FS_PHASE_2_CRAFT_DEFENSES_01 	= 171
QuestManager.quests.FS_PHASE_3_CRAFT_SHIELDS_01 	= 172
QuestManager.quests.FS_PHASE_2_CRAFT_DEFENSES_MAIN 	= 173
QuestManager.quests.FS_PHASE_3_CRAFT_SHIELDS_MAIN 	= 174
QuestManager.quests.LOOT_DATAPAD_2 			= 175
QuestManager.quests.GOT_DATAPAD_2 			= 176
QuestManager.quests.FS_CS_QUEST_FAILED_ESCORT 		= 177
QuestManager.quests.FS_PATROL_QUEST_FINISH 		= 178
QuestManager.quests.FS_MEDIC_PUZZLE_QUEST_FINISH 	= 179
QuestManager.quests.FS_COMBAT_HEALING_FINISH 		= 180
QuestManager.quests.FS_COMBAT_REWARD_PHASE2 		= 181
QuestManager.quests.FS_REFLEX_REWARD_PHASE3 		= 182
QuestManager.quests.FS_DEFEND_WAIT_01 			= 183
QuestManager.quests.FS_DEFEND_WAIT_02 			= 184
QuestManager.quests.FS_CRAFTING4_QUEST_FINISH 		= 185
QuestManager.quests.FS_CRAFT_PUZZLE_QUEST_04 		= 186
QuestManager.quests.FS_CS_QUEST_DONE_NOTIFYONLY 	= 187
QuestManager.quests.HEROIC_SHRINE		 	= 188
QuestManager.quests.HEROIC_SHRINE_BOSS_01	 	= 189
QuestManager.quests.HEROIC_SHRINE_BOSS_02	 	= 190
QuestManager.quests.HEROIC_SHRINE_BOSS_03	 	= 191
QuestManager.quests.HEROIC_SHRINE_BOSS_04	 	= 192
QuestManager.quests.HEROIC_SHRINE_BOSS_05	 	= 193
QuestManager.quests.HEROIC_SHRINE_BOSS_06	 	= 194
QuestManager.quests.HEROIC_AXKVA_MIN		 	= 195
QuestManager.quests.HEROIC_AXKVA_MIN_REBEL	 	= 196
QuestManager.quests.HEROIC_AXKVA_MIN_IMPERIAL	 	= 197
QuestManager.quests.HEROIC_AXKVA_MIN_NEUTRAL	 	= 198
QuestManager.quests.HEROIC_AXKVA_MIN_01		 	= 199
QuestManager.quests.HEROIC_AXKVA_MIN_02		 	= 200
QuestManager.quests.HEROIC_AXKVA_MIN_03		 	= 201
QuestManager.quests.HEROIC_AXKVA_MIN_04		 	= 202
QuestManager.quests.HEROIC_AXKVA_MIN_05		 	= 203
QuestManager.quests.HEROIC_PLASMA		 	= 204
QuestManager.quests.HEROIC_PLASMA_SORUNA	 	= 205
QuestManager.quests.HEROIC_PLASMA_KYLANTHA	 	= 206
QuestManager.quests.HEROIC_PLASMA_01		 	= 207
QuestManager.quests.HEROIC_PLASMA_02		 	= 208
QuestManager.quests.HEROIC_PLASMA_03		 	= 209
QuestManager.quests.HEROIC_PLASMA_04		 	= 210
QuestManager.quests.HEROIC_PLASMA_05		 	= 211
QuestManager.quests.GALAXY_TOUR			 	= 212
QuestManager.quests.GALAXY_TOUR_KUAT		 	= 213
QuestManager.quests.GALAXY_TOUR_MONCAL		 	= 214
QuestManager.quests.GALAXY_TOUR_CORUSCANT	 	= 215
QuestManager.quests.GALAXY_TOUR_CHANDRILA	 	= 216
QuestManager.quests.GALAXY_TOUR_TALUS		 	= 217
QuestManager.quests.GALAXY_TOUR_TAANAB		 	= 218
QuestManager.quests.GALAXY_TOUR_NABOO		 	= 219
QuestManager.quests.GALAXY_TOUR_RORI		 	= 220
QuestManager.quests.GALAXY_TOUR_ENDOR		 	= 221
QuestManager.quests.GALAXY_TOUR_HOTH		 	= 222
QuestManager.quests.GALAXY_TOUR_LOK		 	= 223
QuestManager.quests.GALAXY_TOUR_TATOOINE	 	= 224
QuestManager.quests.GALAXY_TOUR_DANTOOINE	 	= 225
QuestManager.quests.GALAXY_TOUR_DATHOMIR	 	= 226
QuestManager.quests.GALAXY_TOUR_YAVIN4		 	= 227
QuestManager.quests.COLLECTIONS_RESTUSS_DEBRIS		= 228
QuestManager.quests.COLLECTIONS_RESTUSS_DEBRIS_01	= 229
QuestManager.quests.COLLECTIONS_RESTUSS_DEBRIS_02	= 230
QuestManager.quests.COLLECTIONS_RESTUSS_DEBRIS_03	= 231
QuestManager.quests.COLLECTIONS_RESTUSS_DEBRIS_04	= 232
QuestManager.quests.COLLECTIONS_RESTUSS_DEBRIS_05	= 233
QuestManager.quests.COLLECTIONS_RESTUSS_DEBRIS_06	= 234
QuestManager.quests.COLLECTIONS_RESTUSS_DEBRIS_07	= 235
QuestManager.quests.COLLECTIONS_RESTUSS_DEBRIS_08	= 236
QuestManager.quests.SPEEDER_FOR_ME			= 237
QuestManager.quests.SPEEDER_FOR_ME_01			= 238
QuestManager.quests.SPEEDER_FOR_ME_02			= 239
QuestManager.quests.SPEEDER_FOR_ME_03			= 240
QuestManager.quests.RESTUSS_OVERQUEST			= 241
QuestManager.quests.RESTUSS_CALL_HONDO			= 242
QuestManager.quests.RESTUSS_MAYOR			= 243
QuestManager.quests.RESTUSS_MAYOR_GOAL			= 244
QuestManager.quests.RESTUSS_UNIVERSITY_GOAL		= 245
QuestManager.quests.RESTUSS_MERCHANT_GOAL		= 246
QuestManager.quests.RESTUSS_TRAINER_GOAL		= 247
QuestManager.quests.RESTUSS_CANTINA_GOAL		= 248
QuestManager.quests.RESTUSS_HOSPITAL_GOAL		= 249
QuestManager.quests.COLLECTIONS_HOTH_TAUNTAUN		= 250
QuestManager.quests.COLLECTIONS_HOTH_TAUNTAUN_01	= 251
QuestManager.quests.COLLECTIONS_HOTH_TAUNTAUN_02	= 252
QuestManager.quests.COLLECTIONS_HOTH_TAUNTAUN_03	= 253
QuestManager.quests.COLLECTIONS_HOTH_TAUNTAUN_04	= 254
QuestManager.quests.COLLECTIONS_HOTH_TAUNTAUN_05	= 255
QuestManager.quests.COLLECTIONS_HOTH_TAUNTAUN_06	= 256
QuestManager.quests.COLLECTIONS_HOTH_TAUNTAUN_07	= 257
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK		= 258
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK_01	= 259
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK_02	= 260
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK_03	= 261
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK_04	= 262
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK_05	= 263
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK_06	= 264
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK_07	= 265
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK_08	= 266
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK_09	= 267
QuestManager.quests.COLLECTIONS_BUBBLING_ROCK_10	= 268
QuestManager.quests.COLLECTIONS_MIGHT_EMPIRE		= 269
QuestManager.quests.COLLECTIONS_MIGHT_EMPIRE_01		= 270
QuestManager.quests.COLLECTIONS_MIGHT_EMPIRE_02		= 271
QuestManager.quests.COLLECTIONS_MIGHT_EMPIRE_03		= 272
QuestManager.quests.COLLECTIONS_MIGHT_EMPIRE_04		= 273
QuestManager.quests.COLLECTIONS_MIGHT_EMPIRE_05		= 274
QuestManager.quests.COLLECTIONS_MIGHT_EMPIRE_06		= 275
QuestManager.quests.COLLECTIONS_MIGHT_EMPIRE_07		= 276
QuestManager.quests.COLLECTIONS_MIGHT_EMPIRE_08		= 277
QuestManager.quests.COLLECTIONS_MIGHT_EMPIRE_09		= 278
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION	= 279
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION_01	= 280
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION_02	= 281
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION_03	= 282
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION_04	= 283
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION_05	= 284
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION_06	= 285
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION_07	= 286
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION_08	= 287
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION_09	= 288
QuestManager.quests.COLLECTIONS_WILL_OF_REBELLION_10	= 289
QuestManager.quests.COLLECTIONS_BURNING_ROCK		= 290
QuestManager.quests.COLLECTIONS_BURNING_ROCK_01		= 291
QuestManager.quests.COLLECTIONS_BURNING_ROCK_02		= 292
QuestManager.quests.COLLECTIONS_BURNING_ROCK_03		= 293
QuestManager.quests.COLLECTIONS_BURNING_ROCK_04		= 294
QuestManager.quests.COLLECTIONS_BURNING_ROCK_05		= 295
QuestManager.quests.COLLECTIONS_BURNING_ROCK_06		= 296
QuestManager.quests.COLLECTIONS_BURNING_ROCK_07		= 297
QuestManager.quests.COLLECTIONS_BURNING_ROCK_08		= 298
QuestManager.quests.COLLECTIONS_BURNING_ROCK_09		= 299
QuestManager.quests.COLLECTIONS_BURNING_ROCK_10		= 300
QuestManager.quests.COLLECTIONS_STEAMING_ROCK		= 301
QuestManager.quests.COLLECTIONS_STEAMING_ROCK_01	= 302
QuestManager.quests.COLLECTIONS_STEAMING_ROCK_02	= 303
QuestManager.quests.COLLECTIONS_STEAMING_ROCK_03	= 304
QuestManager.quests.COLLECTIONS_STEAMING_ROCK_04	= 305
QuestManager.quests.COLLECTIONS_STEAMING_ROCK_05	= 306
QuestManager.quests.COLLECTIONS_STEAMING_ROCK_06	= 307
QuestManager.quests.COLLECTIONS_STEAMING_ROCK_07	= 308
QuestManager.quests.COLLECTIONS_STEAMING_ROCK_08	= 309
QuestManager.quests.COLLECTIONS_STEAMING_ROCK_09	= 310
QuestManager.quests.COLLECTIONS_STEAMING_ROCK_10	= 311
QuestManager.quests.COLLECTIONS_GLOWING_ROCK		= 312
QuestManager.quests.COLLECTIONS_GLOWING_ROCK_01		= 313
QuestManager.quests.COLLECTIONS_GLOWING_ROCK_02		= 314
QuestManager.quests.COLLECTIONS_GLOWING_ROCK_03		= 315
QuestManager.quests.COLLECTIONS_GLOWING_ROCK_04		= 316
QuestManager.quests.COLLECTIONS_GLOWING_ROCK_05		= 317
QuestManager.quests.COLLECTIONS_GLOWING_ROCK_06		= 318
QuestManager.quests.COLLECTIONS_GLOWING_ROCK_07		= 319
QuestManager.quests.COLLECTIONS_GLOWING_ROCK_08		= 320
QuestManager.quests.COLLECTIONS_GLOWING_ROCK_09		= 321
QuestManager.quests.COLLECTIONS_GLOWING_ROCK_10		= 322
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR	= 323
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR_01	= 324
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR_02	= 325
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR_03	= 326
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR_04	= 327
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR_05	= 328
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR_06	= 329
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR_07	= 330
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR_08	= 331
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR_09	= 332
QuestManager.quests.COLLECTIONS_NR_ASSAULT_ARMOR_10	= 333
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR		= 334
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR_01	= 335
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR_02	= 336
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR_03	= 337
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR_04	= 338
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR_05	= 339
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR_06	= 340
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR_07	= 341
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR_08	= 342
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR_09	= 343
QuestManager.quests.COLLECTIONS_NR_MARINE_ARMOR_10	= 344
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR		= 345
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR_01	= 346
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR_02	= 347
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR_03	= 348
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR_04	= 349
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR_05	= 350
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR_06	= 351
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR_07	= 352
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR_08	= 353
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR_09	= 354
QuestManager.quests.COLLECTIONS_NR_BATTLE_ARMOR_10	= 355
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR	= 356
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR_01	= 357
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR_02	= 358
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR_03	= 359
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR_04	= 360
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR_05	= 361
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR_06	= 362
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR_07	= 363
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR_08	= 364
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR_09	= 365
QuestManager.quests.COLLECTIONS_SCOUT_TROOPER_ARMOR_10	= 366
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR	= 367
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR_01	= 368
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR_02	= 369
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR_03	= 370
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR_04	= 371
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR_05	= 372
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR_06	= 373
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR_07	= 374
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR_08	= 375
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR_09	= 376
QuestManager.quests.COLLECTIONS_SHOCK_TROOPER_ARMOR_10	= 377
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR	= 378
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR_01	= 379
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR_02	= 380
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR_03	= 381
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR_04	= 382
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR_05	= 383
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR_06	= 384
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR_07	= 385
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR_08	= 386
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR_09	= 387
QuestManager.quests.COLLECTIONS_STORMTROOPER_ARMOR_10	= 388
QuestManager.quests.COLLECTIONS_STUFFED_RANCOR		= 389
QuestManager.quests.COLLECTIONS_STUFFED_RANCOR_01	= 390
QuestManager.quests.COLLECTIONS_STUFFED_RANCOR_02	= 391
QuestManager.quests.COLLECTIONS_STUFFED_RANCOR_03	= 392
QuestManager.quests.COLLECTIONS_STUFFED_RANCOR_04	= 393
QuestManager.quests.COLLECTIONS_STUFFED_RANCOR_05	= 394
QuestManager.quests.COLLECTIONS_STUFFED_RANCOR_06	= 395
QuestManager.quests.COLLECTIONS_STUFFED_RANCOR_07	= 396
QuestManager.quests.COLLECTIONS_STUFFED_RANCOR_08	= 397
QuestManager.quests.EMPIRE_DAY_REBEL_PROPAGANDA		= 398
QuestManager.quests.EMPIRE_DAY_IMPERIAL_PROPAGANDA	= 399
QuestManager.quests.EMPIRE_DAY_REBEL_ANTIPROPAGANDA	= 400
QuestManager.quests.EMPIRE_DAY_IMPERIAL_ANTIPROPAGANDA	= 401
QuestManager.quests.EMPIRE_DAY_EWOK_DEFENSE		= 402
QuestManager.quests.EMPIRE_DAY_EWOK_ASSAULT		= 403
QuestManager.quests.EMPIRE_DAY_EWOK_REBEL_PVP		= 404
QuestManager.quests.EMPIRE_DAY_EWOK_IMPERIAL_PVP	= 405
QuestManager.quests.EMPIRE_DAY_REBEL_SPEEDERBIKE	= 406
QuestManager.quests.EMPIRE_DAY_IMPERIAL_SPEEDERBIKE	= 407
QuestManager.quests.COLLECTIONS_STUFFED_DEWBACK		= 408
QuestManager.quests.COLLECTIONS_STUFFED_DEWBACK_01	= 409
QuestManager.quests.COLLECTIONS_STUFFED_DEWBACK_02	= 410
QuestManager.quests.COLLECTIONS_STUFFED_DEWBACK_03	= 411
QuestManager.quests.COLLECTIONS_STUFFED_DEWBACK_04	= 412
QuestManager.quests.COLLECTIONS_STUFFED_DEWBACK_05	= 413
QuestManager.quests.COLLECTIONS_STUFFED_DEWBACK_06	= 414
QuestManager.quests.COLLECTIONS_STUFFED_DEWBACK_07	= 415
QuestManager.quests.COLLECTIONS_STUFFED_DEWBACK_08	= 416
QuestManager.quests.COLLECTIONS_STUFFED_TAUNTAUN	= 417
QuestManager.quests.COLLECTIONS_STUFFED_TAUNTAUN_01	= 418
QuestManager.quests.COLLECTIONS_STUFFED_TAUNTAUN_02	= 419
QuestManager.quests.COLLECTIONS_STUFFED_TAUNTAUN_03	= 420
QuestManager.quests.COLLECTIONS_STUFFED_TAUNTAUN_04	= 421
QuestManager.quests.COLLECTIONS_STUFFED_TAUNTAUN_05	= 422
QuestManager.quests.COLLECTIONS_STUFFED_TAUNTAUN_06	= 423
QuestManager.quests.COLLECTIONS_STUFFED_TAUNTAUN_07	= 424
QuestManager.quests.COLLECTIONS_STUFFED_TAUNTAUN_08	= 425
QuestManager.quests.COLLECTIONS_STUFFED_WAMPA		= 426
QuestManager.quests.COLLECTIONS_STUFFED_WAMPA_01	= 427
QuestManager.quests.COLLECTIONS_STUFFED_WAMPA_02	= 428
QuestManager.quests.COLLECTIONS_STUFFED_WAMPA_03	= 429
QuestManager.quests.COLLECTIONS_STUFFED_WAMPA_04	= 430
QuestManager.quests.COLLECTIONS_STUFFED_WAMPA_05	= 431
QuestManager.quests.COLLECTIONS_STUFFED_WAMPA_06	= 432
QuestManager.quests.COLLECTIONS_STUFFED_WAMPA_07	= 433
QuestManager.quests.COLLECTIONS_STUFFED_WAMPA_08	= 434
QuestManager.quests.COLLECTIONS_STUFFED_WAMPA_09	= 435
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION	= 436
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_01	= 437
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_02	= 438
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_03	= 439
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_04	= 440
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_05	= 441
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_06	= 442
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_07	= 443
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_08	= 444
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_09	= 445
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_10	= 446
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_11	= 447
QuestManager.quests.COLLECTIONS_DURNI_INFESTATION_12	= 448
QuestManager.quests.COLLECTIONS_GLASS_SHELVING		= 449
QuestManager.quests.COLLECTIONS_GLASS_SHELVING_01	= 450
QuestManager.quests.COLLECTIONS_GLASS_SHELVING_02	= 451
QuestManager.quests.COLLECTIONS_GLASS_SHELVING_03	= 452
QuestManager.quests.COLLECTIONS_GLASS_SHELVING_04	= 453
QuestManager.quests.COLLECTIONS_GLASS_SHELVING_05	= 454
QuestManager.quests.COLLECTIONS_GLASS_SHELVING_06	= 455
QuestManager.quests.COLLECTIONS_GLASS_SHELVING_07	= 456
QuestManager.quests.COLLECTIONS_GLASS_SHELVING_08	= 457
QuestManager.quests.COLLECTIONS_GLASS_SHELVING_09	= 458
QuestManager.quests.COLLECTIONS_GLASS_SHELVING_10	= 459
QuestManager.quests.COLLECTIONS_HOTH_METEORITE		= 460
QuestManager.quests.COLLECTIONS_HOTH_METEORITE_01	= 461
QuestManager.quests.COLLECTIONS_HOTH_METEORITE_02	= 462
QuestManager.quests.COLLECTIONS_HOTH_METEORITE_03	= 463
QuestManager.quests.COLLECTIONS_HOTH_METEORITE_04	= 464
QuestManager.quests.COLLECTIONS_HOTH_METEORITE_05	= 465
QuestManager.quests.COLLECTIONS_HOTH_METEORITE_06	= 466
QuestManager.quests.COLLECTIONS_HOTH_METEORITE_07	= 467
QuestManager.quests.COLLECTIONS_HOTH_METEORITE_08	= 468
QuestManager.quests.COLLECTIONS_HOTH_METEORITE_09	= 469
QuestManager.quests.COLLECTIONS_HOTH_METEORITE_10	= 470
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE	= 471
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE_01	= 472
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE_02	= 473
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE_03	= 474
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE_04	= 475
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE_05	= 476
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE_06	= 477
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE_07	= 478
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE_08	= 479
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE_09	= 480
QuestManager.quests.COLLECTIONS_ANTIQUE_SNIPER_RIFLE_10	= 481
QuestManager.quests.COLLECTIONS_TATOOINE_LORE		= 482
QuestManager.quests.COLLECTIONS_TATOOINE_LORE_01	= 483
QuestManager.quests.COLLECTIONS_TATOOINE_LORE_02	= 484
QuestManager.quests.COLLECTIONS_TATOOINE_LORE_03	= 485
QuestManager.quests.COLLECTIONS_TATOOINE_LORE_04	= 486
QuestManager.quests.COLLECTIONS_TATOOINE_LORE_05	= 487
QuestManager.quests.COLLECTIONS_TATOOINE_LORE_06	= 488
QuestManager.quests.COLLECTIONS_TATOOINE_LORE_07	= 489
QuestManager.quests.COLLECTIONS_TATOOINE_LORE_08	= 490
QuestManager.quests.COLLECTIONS_TATOOINE_LORE_09	= 491
QuestManager.quests.COLLECTIONS_TATOOINE_LORE_10	= 492
QuestManager.quests.COLLECTIONS_FISHING_EXPEDITION	= 493
QuestManager.quests.COLLECTIONS_FISHING_EXPEDITION_01	= 494
QuestManager.quests.COLLECTIONS_FISHING_EXPEDITION_02	= 495
QuestManager.quests.COLLECTIONS_FISHING_EXPEDITION_03	= 496
QuestManager.quests.COLLECTIONS_FISHING_EXPEDITION_04	= 497
QuestManager.quests.COLLECTIONS_FISHING_EXPEDITION_05	= 498
QuestManager.quests.COLLECTIONS_FISHING_EXPEDITION_06	= 499
QuestManager.quests.COLLECTIONS_FISHING_EXPEDITION_07	= 500
QuestManager.quests.COLLECTIONS_FISHING_EXPEDITION_08	= 501
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE		= 502
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE_01	= 503
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE_02	= 504
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE_03	= 505
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE_04	= 506
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE_05	= 507
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE_06	= 508
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE_07	= 509
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE_08	= 510
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE_09	= 511
QuestManager.quests.COLLECTIONS_ANTIQUE_CARBINE_10	= 512
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL		= 513
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL_01	= 514
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL_02	= 515
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL_03	= 516
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL_04	= 517
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL_05	= 518
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL_06	= 519
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL_07	= 520
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL_08	= 521
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL_09	= 522
QuestManager.quests.COLLECTIONS_ANTIQUE_PISTOL_10	= 523
QuestManager.quests.COLLECTIONS_DUST_DURNIS		= 524
QuestManager.quests.COLLECTIONS_DUST_DURNIS_01		= 525
QuestManager.quests.COLLECTIONS_DUST_DURNIS_02		= 526
QuestManager.quests.COLLECTIONS_DUST_DURNIS_03		= 527
QuestManager.quests.COLLECTIONS_DUST_DURNIS_04		= 528
QuestManager.quests.COLLECTIONS_DUST_DURNIS_05		= 529
QuestManager.quests.COLLECTIONS_DUST_DURNIS_06		= 530
QuestManager.quests.COLLECTIONS_DUST_DURNIS_07		= 531
QuestManager.quests.COLLECTIONS_DUST_DURNIS_08		= 532
QuestManager.quests.COLLECTIONS_DUST_DURNIS_09		= 533
QuestManager.quests.COLLECTIONS_DUST_DURNIS_10		= 534
QuestManager.quests.COLLECTIONS_CORELLIA_LORE		= 535
QuestManager.quests.COLLECTIONS_CORELLIA_LORE_01	= 536
QuestManager.quests.COLLECTIONS_CORELLIA_LORE_02	= 537
QuestManager.quests.COLLECTIONS_CORELLIA_LORE_03	= 538
QuestManager.quests.COLLECTIONS_CORELLIA_LORE_04	= 539
QuestManager.quests.COLLECTIONS_CORELLIA_LORE_05	= 540
QuestManager.quests.COLLECTIONS_CORELLIA_LORE_06	= 541
QuestManager.quests.COLLECTIONS_CORELLIA_LORE_07	= 542
QuestManager.quests.COLLECTIONS_CORELLIA_LORE_08	= 543
QuestManager.quests.COLLECTIONS_CORELLIA_LORE_09	= 544
QuestManager.quests.COLLECTIONS_CORELLIA_LORE_10	= 545
QuestManager.quests.COLLECTIONS_NABOO_LORE		= 546
QuestManager.quests.COLLECTIONS_NABOO_LORE_01		= 547
QuestManager.quests.COLLECTIONS_NABOO_LORE_02		= 548
QuestManager.quests.COLLECTIONS_NABOO_LORE_03		= 549
QuestManager.quests.COLLECTIONS_NABOO_LORE_04		= 550
QuestManager.quests.COLLECTIONS_NABOO_LORE_05		= 551
QuestManager.quests.COLLECTIONS_NABOO_LORE_06		= 552
QuestManager.quests.COLLECTIONS_NABOO_LORE_07		= 553
QuestManager.quests.COLLECTIONS_NABOO_LORE_08		= 554
QuestManager.quests.COLLECTIONS_NABOO_LORE_09		= 555
QuestManager.quests.COLLECTIONS_NABOO_LORE_10		= 556
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM		= 557
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM_01	= 558
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM_02	= 559
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM_03	= 560
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM_04	= 561
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM_05	= 562
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM_06	= 563
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM_07	= 564
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM_08	= 565
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM_09	= 566
QuestManager.quests.COLLECTIONS_FAMILY_HEIRLOOM_10	= 567
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER	= 568
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER_01	= 569
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER_02	= 570
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER_03	= 571
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER_04	= 572
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER_05	= 573
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER_06	= 574
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER_07	= 575
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER_08	= 576
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER_09	= 577
QuestManager.quests.COLLECTIONS_ANTIQUE_KNUCKLER_10	= 578
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD		= 579
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD_01	= 580
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD_02	= 581
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD_03	= 582
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD_04	= 583
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD_05	= 584
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD_06	= 585
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD_07	= 586
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD_08	= 587
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD_09	= 588
QuestManager.quests.COLLECTIONS_ANTIQUE_SWORD_10	= 589
QuestManager.quests.COLLECTIONS_DAISYPICKING		= 590
QuestManager.quests.COLLECTIONS_DAISYPICKING_01		= 591
QuestManager.quests.COLLECTIONS_DAISYPICKING_02		= 592
QuestManager.quests.COLLECTIONS_DAISYPICKING_03		= 593
QuestManager.quests.COLLECTIONS_DAISYPICKING_04		= 594
QuestManager.quests.COLLECTIONS_DAISYPICKING_05		= 595
QuestManager.quests.COLLECTIONS_DAISYPICKING_06		= 596
QuestManager.quests.COLLECTIONS_DAISYPICKING_07		= 597
QuestManager.quests.COLLECTIONS_DAISYPICKING_08		= 598
QuestManager.quests.COLLECTIONS_RORI_LORE		= 599
QuestManager.quests.COLLECTIONS_RORI_LORE_01		= 600
QuestManager.quests.COLLECTIONS_RORI_LORE_02		= 601
QuestManager.quests.COLLECTIONS_RORI_LORE_03		= 602
QuestManager.quests.COLLECTIONS_RORI_LORE_04		= 603
QuestManager.quests.COLLECTIONS_RORI_LORE_05		= 604
QuestManager.quests.COLLECTIONS_RORI_LORE_06		= 605
QuestManager.quests.COLLECTIONS_RORI_LORE_07		= 606
QuestManager.quests.COLLECTIONS_RORI_LORE_08		= 607
QuestManager.quests.COLLECTIONS_RORI_LORE_09		= 608
QuestManager.quests.COLLECTIONS_RORI_LORE_10		= 609
QuestManager.quests.COLLECTIONS_KUAT_LORE		= 610
QuestManager.quests.COLLECTIONS_KUAT_LORE_01		= 611
QuestManager.quests.COLLECTIONS_KUAT_LORE_02		= 612
QuestManager.quests.COLLECTIONS_KUAT_LORE_03		= 613
QuestManager.quests.COLLECTIONS_KUAT_LORE_04		= 614
QuestManager.quests.COLLECTIONS_KUAT_LORE_05		= 615
QuestManager.quests.COLLECTIONS_KUAT_LORE_06		= 616
QuestManager.quests.COLLECTIONS_KUAT_LORE_07		= 617
QuestManager.quests.COLLECTIONS_KUAT_LORE_08		= 618
QuestManager.quests.COLLECTIONS_KUAT_LORE_09		= 619
QuestManager.quests.COLLECTIONS_KUAT_LORE_10		= 620
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD	= 621
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD_01	= 622
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD_02	= 623
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD_03	= 624
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD_04	= 625
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD_05	= 626
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD_06	= 627
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD_07	= 628
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD_08	= 629
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD_09	= 630
QuestManager.quests.COLLECTIONS_ANTIQUE_2H_SWORD_10	= 631
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM		= 632
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM_01	= 633
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM_02	= 634
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM_03	= 635
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM_04	= 636
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM_05	= 637
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM_06	= 638
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM_07	= 639
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM_08	= 640
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM_09	= 641
QuestManager.quests.COLLECTIONS_ANTIQUE_POLEARM_10	= 642
QuestManager.quests.COLLECTIONS_BEETLEBUSTER		= 643
QuestManager.quests.COLLECTIONS_BEETLEBUSTER_01		= 644
QuestManager.quests.COLLECTIONS_BEETLEBUSTER_02		= 645
QuestManager.quests.COLLECTIONS_BEETLEBUSTER_03		= 646
QuestManager.quests.COLLECTIONS_BEETLEBUSTER_04		= 647
QuestManager.quests.COLLECTIONS_BEETLEBUSTER_05		= 648
QuestManager.quests.COLLECTIONS_BEETLEBUSTER_06		= 649
QuestManager.quests.COLLECTIONS_BEETLEBUSTER_07		= 650
QuestManager.quests.COLLECTIONS_BEETLEBUSTER_08		= 651
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE		= 652
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE_01	= 653
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE_02	= 654
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE_03	= 655
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE_04	= 656
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE_05	= 657
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE_06	= 658
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE_07	= 659
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE_08	= 660
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE_09	= 661
QuestManager.quests.COLLECTIONS_CORUSCANT_LORE_10	= 662
QuestManager.quests.COLLECTIONS_LOK_LORE		= 663
QuestManager.quests.COLLECTIONS_LOK_LORE_01		= 664
QuestManager.quests.COLLECTIONS_LOK_LORE_02		= 665
QuestManager.quests.COLLECTIONS_LOK_LORE_03		= 666
QuestManager.quests.COLLECTIONS_LOK_LORE_04		= 667
QuestManager.quests.COLLECTIONS_LOK_LORE_05		= 668
QuestManager.quests.COLLECTIONS_LOK_LORE_06		= 669
QuestManager.quests.COLLECTIONS_LOK_LORE_07		= 670
QuestManager.quests.COLLECTIONS_LOK_LORE_08		= 671
QuestManager.quests.COLLECTIONS_LOK_LORE_09		= 672
QuestManager.quests.COLLECTIONS_LOK_LORE_10		= 673
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES	= 674
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES_01	= 675
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES_02	= 676
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES_03	= 677
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES_04	= 678
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES_05	= 679
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES_06	= 680
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES_07	= 681
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES_08	= 682
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES_09	= 683
QuestManager.quests.COLLECTIONS_MEDICAL_SUPPLIES_10	= 684
QuestManager.quests.COLLECTIONS_TAANAB_LORE		= 685
QuestManager.quests.COLLECTIONS_TAANAB_LORE_01		= 686
QuestManager.quests.COLLECTIONS_TAANAB_LORE_02		= 687
QuestManager.quests.COLLECTIONS_TAANAB_LORE_03		= 688
QuestManager.quests.COLLECTIONS_TAANAB_LORE_04		= 689
QuestManager.quests.COLLECTIONS_TAANAB_LORE_05		= 690
QuestManager.quests.COLLECTIONS_TAANAB_LORE_06		= 691
QuestManager.quests.COLLECTIONS_TAANAB_LORE_07		= 692
QuestManager.quests.COLLECTIONS_TAANAB_LORE_08		= 693
QuestManager.quests.COLLECTIONS_TAANAB_LORE_09		= 694
QuestManager.quests.COLLECTIONS_TAANAB_LORE_10		= 695
QuestManager.quests.COLLECTIONS_PAZAAK_CARDS		= 696
QuestManager.quests.COLLECTIONS_PAZAAK_CARDS_01		= 697
QuestManager.quests.COLLECTIONS_PAZAAK_CARDS_02		= 698
QuestManager.quests.COLLECTIONS_PAZAAK_CARDS_03		= 699
QuestManager.quests.COLLECTIONS_PAZAAK_CARDS_04		= 700
QuestManager.quests.COLLECTIONS_PAZAAK_CARDS_05		= 701
QuestManager.quests.COLLECTIONS_PAZAAK_CARDS_06		= 702
QuestManager.quests.COLLECTIONS_YAVIN_LORE		= 703
QuestManager.quests.COLLECTIONS_YAVIN_LORE_01		= 704
QuestManager.quests.COLLECTIONS_YAVIN_LORE_02		= 705
QuestManager.quests.COLLECTIONS_YAVIN_LORE_03		= 706
QuestManager.quests.COLLECTIONS_YAVIN_LORE_04		= 707
QuestManager.quests.COLLECTIONS_YAVIN_LORE_05		= 708
QuestManager.quests.COLLECTIONS_YAVIN_LORE_06		= 709
QuestManager.quests.COLLECTIONS_YAVIN_LORE_07		= 710
QuestManager.quests.COLLECTIONS_YAVIN_LORE_08		= 711
QuestManager.quests.COLLECTIONS_YAVIN_LORE_09		= 712
QuestManager.quests.COLLECTIONS_YAVIN_LORE_10		= 713
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE		= 714
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE_01	= 715
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE_02	= 716
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE_03	= 717
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE_04	= 718
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE_05	= 719
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE_06	= 720
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE_07	= 721
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE_08	= 722
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE_09	= 723
QuestManager.quests.COLLECTIONS_DANTOOINE_LORE_10	= 724
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE		= 725
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE_01	= 726
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE_02	= 727
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE_03	= 728
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE_04	= 729
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE_05	= 730
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE_06	= 731
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE_07	= 732
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE_08	= 733
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE_09	= 734
QuestManager.quests.COLLECTIONS_DATHOMIR_LORE_10	= 735
QuestManager.quests.COLLECTIONS_ENDOR_LORE		= 736
QuestManager.quests.COLLECTIONS_ENDOR_LORE_01		= 737
QuestManager.quests.COLLECTIONS_ENDOR_LORE_02		= 738
QuestManager.quests.COLLECTIONS_ENDOR_LORE_03		= 739
QuestManager.quests.COLLECTIONS_ENDOR_LORE_04		= 740
QuestManager.quests.COLLECTIONS_ENDOR_LORE_05		= 741
QuestManager.quests.COLLECTIONS_ENDOR_LORE_06		= 742
QuestManager.quests.COLLECTIONS_ENDOR_LORE_07		= 743
QuestManager.quests.COLLECTIONS_ENDOR_LORE_08		= 744
QuestManager.quests.COLLECTIONS_ENDOR_LORE_09		= 745
QuestManager.quests.COLLECTIONS_ENDOR_LORE_10		= 746
QuestManager.quests.KILL_COLLECTIONS_NORULAC		= 747
QuestManager.quests.KILL_COLLECTIONS_NORULAC_01		= 748
QuestManager.quests.KILL_COLLECTIONS_NORULAC_02		= 749
QuestManager.quests.KILL_COLLECTIONS_NORULAC_03		= 750
QuestManager.quests.KILL_COLLECTIONS_NORULAC_04		= 751
QuestManager.quests.KILL_COLLECTIONS_NORULAC_05		= 752
QuestManager.quests.JEDI_QUIXOTE			= 753
QuestManager.quests.JEDI_QUIXOTE_01			= 754
QuestManager.quests.JEDI_QUIXOTE_02			= 755
QuestManager.quests.JEDI_QUIXOTE_03			= 756
QuestManager.quests.JEDI_QUIXOTE_04			= 757
QuestManager.quests.JEDI_QUIXOTE_05			= 758
QuestManager.quests.JEDI_QUIXOTE_06			= 759
QuestManager.quests.JEDI_QUIXOTE_07			= 760
QuestManager.quests.JEDI_QUIXOTE_08			= 761
QuestManager.quests.JEDI_QUIXOTE_09			= 762
QuestManager.quests.JEDI_QUIXOTE_10			= 763
QuestManager.quests.COLLECTIONS_FORCE_LORE		= 764
QuestManager.quests.COLLECTIONS_FORCE_LORE_01		= 765
QuestManager.quests.COLLECTIONS_FORCE_LORE_02		= 766
QuestManager.quests.COLLECTIONS_FORCE_LORE_03		= 767
QuestManager.quests.COLLECTIONS_FORCE_LORE_04		= 768
QuestManager.quests.COLLECTIONS_FORCE_LORE_05		= 769
QuestManager.quests.COLLECTIONS_FORCE_LORE_06		= 770
QuestManager.quests.COLLECTIONS_FORCE_LORE_07		= 771
QuestManager.quests.COLLECTIONS_FORCE_LORE_08		= 772
QuestManager.quests.JEDI_LIGHTSABER_TRAINING		= 773
QuestManager.quests.JEDI_LIGHTSABER_01_A		= 774
QuestManager.quests.JEDI_LIGHTSABER_01_B		= 775
QuestManager.quests.JEDI_LIGHTSABER_02_A		= 776
QuestManager.quests.JEDI_LIGHTSABER_02_B		= 777
QuestManager.quests.JEDI_LIGHTSABER_03_A		= 778
QuestManager.quests.JEDI_LIGHTSABER_03_B		= 779
QuestManager.quests.JEDI_LIGHTSABER_04_A		= 780
QuestManager.quests.JEDI_LIGHTSABER_04_B		= 781
QuestManager.quests.JEDI_LIGHTSABER_04_C		= 782
QuestManager.quests.JEDI_DEFENSE_TRAINING		= 783
QuestManager.quests.JEDI_DEFENSE_01_A			= 784
QuestManager.quests.JEDI_DEFENSE_01_B			= 785
QuestManager.quests.JEDI_DEFENSE_01_C			= 786
QuestManager.quests.JEDI_DEFENSE_02_A			= 787
QuestManager.quests.JEDI_DEFENSE_02_B			= 788
QuestManager.quests.JEDI_DEFENSE_02_C			= 789
QuestManager.quests.JEDI_DEFENSE_03_A			= 790
QuestManager.quests.JEDI_DEFENSE_03_B			= 791
QuestManager.quests.JEDI_DEFENSE_04_A			= 792
QuestManager.quests.JEDI_DEFENSE_04_B			= 793
QuestManager.quests.JEDI_DEFENSE_04_C			= 794
QuestManager.quests.JEDI_OFFENSE_TRAINING		= 795
QuestManager.quests.JEDI_OFFENSE_01_A			= 796
QuestManager.quests.JEDI_OFFENSE_01_B			= 797
QuestManager.quests.JEDI_OFFENSE_01_C			= 798
QuestManager.quests.JEDI_OFFENSE_02_A			= 799
QuestManager.quests.JEDI_OFFENSE_02_B			= 800
QuestManager.quests.JEDI_OFFENSE_02_C			= 801
QuestManager.quests.JEDI_OFFENSE_03_A			= 802
QuestManager.quests.JEDI_OFFENSE_03_B			= 803
QuestManager.quests.JEDI_OFFENSE_03_C			= 804
QuestManager.quests.JEDI_OFFENSE_04_A			= 805
QuestManager.quests.JEDI_OFFENSE_04_B			= 806
QuestManager.quests.JEDI_OFFENSE_04_C			= 807
QuestManager.quests.JEDI_MYSTIC_TRAINING		= 808
QuestManager.quests.JEDI_MYSTIC_01_A			= 809
QuestManager.quests.JEDI_MYSTIC_01_B			= 810
QuestManager.quests.JEDI_MYSTIC_02_A			= 811
QuestManager.quests.JEDI_MYSTIC_02_B			= 812
QuestManager.quests.JEDI_MYSTIC_03_A			= 813
QuestManager.quests.JEDI_MYSTIC_03_B			= 814
QuestManager.quests.JEDI_MYSTIC_03_C			= 815
QuestManager.quests.JEDI_MYSTIC_04_A			= 816
QuestManager.quests.JEDI_MYSTIC_04_B			= 817
QuestManager.quests.JEDI_MYSTIC_04_C			= 818
QuestManager.quests.SITH_LIGHTSABER_01_A		= 819
QuestManager.quests.SITH_LIGHTSABER_01_B		= 820
QuestManager.quests.SITH_LIGHTSABER_02_A		= 821
QuestManager.quests.SITH_LIGHTSABER_02_B		= 822
QuestManager.quests.SITH_LIGHTSABER_03_A		= 823
QuestManager.quests.SITH_LIGHTSABER_03_B		= 824
QuestManager.quests.SITH_LIGHTSABER_04_A		= 825
QuestManager.quests.SITH_LIGHTSABER_04_B		= 826
QuestManager.quests.SITH_LIGHTSABER_04_C		= 827
QuestManager.quests.SITH_DEFENSE_01_A			= 828
QuestManager.quests.SITH_DEFENSE_01_B			= 829
QuestManager.quests.SITH_DEFENSE_01_C			= 830
QuestManager.quests.SITH_DEFENSE_02_A			= 831
QuestManager.quests.SITH_DEFENSE_02_B			= 832
QuestManager.quests.SITH_DEFENSE_02_C			= 833
QuestManager.quests.SITH_DEFENSE_03_A			= 834
QuestManager.quests.SITH_DEFENSE_03_B			= 835
QuestManager.quests.SITH_DEFENSE_04_A			= 836
QuestManager.quests.SITH_DEFENSE_04_B			= 837
QuestManager.quests.SITH_DEFENSE_04_C			= 838
QuestManager.quests.SITH_OFFENSE_01_A			= 839
QuestManager.quests.SITH_OFFENSE_01_B			= 840
QuestManager.quests.SITH_OFFENSE_01_C			= 841
QuestManager.quests.SITH_OFFENSE_02_A			= 842
QuestManager.quests.SITH_OFFENSE_02_B			= 843
QuestManager.quests.SITH_OFFENSE_02_C			= 844
QuestManager.quests.SITH_OFFENSE_03_A			= 845
QuestManager.quests.SITH_OFFENSE_03_B			= 846
QuestManager.quests.SITH_OFFENSE_03_C			= 847
QuestManager.quests.SITH_OFFENSE_04_A			= 848
QuestManager.quests.SITH_OFFENSE_04_B			= 849
QuestManager.quests.SITH_OFFENSE_04_C			= 850
QuestManager.quests.SITH_MYSTIC_01_A			= 851
QuestManager.quests.SITH_MYSTIC_01_B			= 852
QuestManager.quests.SITH_MYSTIC_02_A			= 853
QuestManager.quests.SITH_MYSTIC_02_B			= 854
QuestManager.quests.SITH_MYSTIC_03_A			= 855
QuestManager.quests.SITH_MYSTIC_03_B			= 856
QuestManager.quests.SITH_MYSTIC_03_C			= 857
QuestManager.quests.SITH_MYSTIC_04_A			= 858
QuestManager.quests.SITH_MYSTIC_04_B			= 859
QuestManager.quests.SITH_MYSTIC_04_C			= 860


return QuestManager
