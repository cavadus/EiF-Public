--EiF 2020
--Please do not use without permission

CoverFireCommand = {
        name = "coverfire",
	damageMultiplier = 1.25,
	speedMultiplier = 1.0,
	healthCostMultiplier = 0,
	actionCostMultiplier = 45,
	mindCostMultiplier = 0,
  	accuracyBonus = 30,

        --cone angle isn't strictly documented, leaving it alone
	coneAngle = 60,
	coneAction = true,
	stateEffects = {
	  StateEffect( 
		NEXTATTACKDELAY_EFFECT, 
		{ "nextAttackDelayRecovery" }, 
		{ "warcry_defense" }, 
		{}, 
		100, 
		0, 
		20 
	  ),
	  StateEffect(
		INTIMIDATE_EFFECT,
		{ "intimidate_defense" },
		{ "jedi_state_defense", "resistance_states" },
		{},
		100,
		0,
		120
	  )
	},

	poolsToDamage = RANDOM_ATTRIBUTE,

	animation = "fire_heavy_cannon_cone_1",

	animType = GENERATE_INTENSITY,

	combatSpam = "sprayshot",

	weaponType = ROTARYWEAPON,

	range = -1
}

AddCommand(CoverFireCommand)
