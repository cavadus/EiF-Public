--EiF 2020
--Please do not use without permission

OverwhelmingFireCommand = {
        name = "overwhelmingfire",

	damageMultiplier = 2.0,
	speedMultiplier = 1.5,
	healthCostMultiplier = 0,
	actionCostMultiplier = 45,
	mindCostMultiplier = 0,
	accuracyBonus = 25,

	animation = "fire_area",
	animType = GENERATE_INTENSITY,

	combatSpam = "a_auto",

	coneAngle = 60,
	coneAction = true,

	poolsToDamage = HEALTH_ATTRIBUTE,

	weaponType = ROTARYWEAPON,

	range = -1
}

AddCommand(OverwhelmingFireCommand)
