--EiF 2020
--Please do not use without permission

SuppressiveFireCommand = {
        name = "suppressivefire",
	damageMultiplier = 1.25,
	speedMultiplier = 1.0,
	healthCostMultiplier = 0,
	actionCostMultiplier = 45,
	mindCostMultiplier = 0,
  	accuracyBonus = 30,

        --cone angle isn't strictly documented, leaving it alone
	coneAngle = 60,
	coneAction = true,
	stateEffects = {
	  StateEffect(
		POSTUREDOWN_EFFECT,
		{ "postureDownRecovery" },
		{ "posture_change_down_defense" },
		{},
		100,
		0,
		0
	  ),
	  StateEffect(
		STUN_EFFECT,
		{},
		{ "stun_defense" },
		{ "jedi_state_defense", "resistance_states" },
		90,
		0,
		10
	  )
	},

	poolsToDamage = RANDOM_ATTRIBUTE,

	animation = "fire_heavy_cannon_cone_1",

	animType = GENERATE_INTENSITY,

	combatSpam = "sup_fire",

	weaponType = ROTARYWEAPON,

	range = -1
}

AddCommand(SuppressiveFireCommand)
