/*
 * LightsaberCrystalComponentImplementation.cpp
 *
 *      Author: Katherine
 */

#include "engine/engine.h"
#include "server/zone/objects/player/sui/messagebox/SuiMessageBox.h"
#include "server/zone/objects/tangible/component/lightsaber/LightsaberCrystalComponent.h"
#include "server/zone/packets/object/ObjectMenuResponse.h"
#include "server/zone/objects/tangible/wearables/WearableContainerObject.h"
#include "server/zone/packets/scene/AttributeListMessage.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "server/zone/objects/player/sui/callbacks/LightsaberCrystalTuneSuiCallback.h"
#include "server/zone/objects/tangible/weapon/WeaponObject.h"
#include "server/zone/managers/stringid/StringIdManager.h"
#include "server/zone/managers/loot/CrystalData.h"
#include "server/zone/managers/loot/LootManager.h"
#include "server/zone/ZoneServer.h"

void LightsaberCrystalComponentImplementation::initializeTransientMembers() {
	ComponentImplementation::initializeTransientMembers();

	setLoggingName("LightsaberCrystalComponent");
}

void LightsaberCrystalComponentImplementation::notifyLoadFromDatabase() {
	// Randomize item level and stats for existing crystals based on original quality value
	// TODO: Remove this on a server wipe when old variables are removed
	if ((isKraytPearl() || isPowerCrystal()) && (minimumDamage != maximumDamage || itemLevel == 0)) {
		int randomColor = System::random(7) + 120;
		setColor(randomColor);
		updateCrystal(randomColor);

		if (quality == POOR)
			itemLevel = 1 + System::random(38); // 1-39
		else if (quality == FAIR)
			itemLevel = 40 + System::random(29); // 40-69
		else if (quality == GOOD)
			itemLevel = 70 + System::random(29); // 70-99
		else if (quality == QUALITY)
			itemLevel = 100 + System::random(39); // 100-139
		else if (quality == SELECT)
			itemLevel = 140 + System::random(79); // 140-219
		else if (quality == PREMIUM)
			itemLevel = 220 + System::random(109); // 220-329
		else
			itemLevel = 330 + System::random(20);

		attackSpeed = 0.0;
		minimumDamage = 0.0;
		maximumDamage = 0.0;
		sacHealth = 0.0;
		sacAction = 0.0;
		sacMind = 0.0;
		woundChance = 0.0;
		idealAccuracy = 0.0;
		pointBlankAccuracy = 0.0;
		
		generateCrystalStats();
	}

	TangibleObjectImplementation::notifyLoadFromDatabase();
}

void LightsaberCrystalComponentImplementation::generateCrystalStats() {
	ManagedReference<LootManager*> lootManager = getZoneServer()->getLootManager();

	if (lootManager == nullptr)
		return;

	String crystalLoot = "lightsaber_module_force_crystal";

	if (isKraytPearl())
		crystalLoot = "lightsaber_module_krayt_dragon_pearl";

	const CrystalData* crystalData = lootManager->getCrystalData(crystalLoot);

	if (crystalData == nullptr) {
		error("Unable to find crystal stats for " + getObjectTemplate()->getTemplateFileName());
		return;
	}

	int minStat = crystalData->getMinHitpoints();
	int maxStat = crystalData->getMaxHitpoints();

	setMaxCondition(getRandomizedStat(minStat, maxStat, itemLevel));

	//EiF lightsaber revamp, 2021
	if (isKraytPearl()){
		int roll = System::random(1);
		if ( roll <= 0 ){
			float minFloatStat = crystalData->getMinDamage();
			float maxFloatStat = crystalData->getMaxDamage();
			damage = Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);
		} else {
			float minFloatStat = 0.0;
			float maxFloatStat = 0.0;
			damage = Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);
			
			minFloatStat = crystalData->getMinAttackSpeed();
			maxFloatStat = crystalData->getMaxAttackSpeed();
			attackSpeed = Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);
		}
	} else if (isPowerCrystal()) {
		
		// Determine Primary Stat
		Vector<String> primaryStats{ "attackSpeed", "actionAttackCost", "pointBlankAccuracy"};
		String primaryStat = primaryStats.get(System::random(primaryStats.size()-1));

		if (primaryStat == "mindAttackCost") {
			float minFloatStat  = crystalData->getMinMindSac();
			float maxFloatStat = crystalData->getMaxMindSac();
			sacMind = Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);
		} else if ( primaryStat == "attackSpeed") {
			float minFloatStat = crystalData->getMinAttackSpeed();
			float maxFloatStat = crystalData->getMaxAttackSpeed();

			attackSpeed = Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);
		} else if ( primaryStat == "actionAttackCost") {
			float minFloatStat  = crystalData->getMinActionSac();
			float maxFloatStat = crystalData->getMaxActionSac();

			sacAction = Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);
		} else if ( primaryStat == "pointBlankAccuracy") {
			float minFloatStat  = crystalData->getMinPointBlankAccuracy();
			float maxFloatStat = crystalData->getMaxPointBlankAccuracy();

			pointBlankAccuracy = Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);
		}	

		// Determine Secondary Stats
		Vector<String> secondaryStats{ "idealAccuracy", "pointBlankAccuracy", "woundsRatio", "damage"};
		int numberOfSecondaries = itemLevel / 50;
		damage = 0;
		for (int i = 0; i < numberOfSecondaries && secondaryStats.size() >= 1; i++ ){
			int secondaryIndex = System::random(secondaryStats.size()-1);
			String secondaryStat = secondaryStats.get(secondaryIndex);
			
			secondaryStats.remove(secondaryIndex);
			if ( secondaryStat == "idealAccuracy") {
				float minFloatStat  = crystalData->getMinIdealAccuracy();
				float maxFloatStat = crystalData->getMaxIdealAccuracy();

				idealAccuracy =  Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);			
			} else if ( secondaryStat ==  "pointBlankAccuracy") {
				if ( primaryStat == secondaryStat){
					i--;
				} else {
					float minFloatStat  = crystalData->getMinPointBlankAccuracy()/2;
					float maxFloatStat = crystalData->getMaxPointBlankAccuracy()/2;

					pointBlankAccuracy =  Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);					
				}
			} else if ( secondaryStat == "woundsRatio") {
				float minFloatStat  = crystalData->getMinWoundChance();
				float maxFloatStat = crystalData->getMaxWoundChance();

				woundChance =  Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);
			} else if ( secondaryStat == "damage") {
				float minFloatStat  = crystalData->getMinDamage();
				float maxFloatStat = crystalData->getMaxDamage();
				float damageRoll = Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);
				
				damage =  Math::max(damageRoll,(float)0.0);
				i++;
			}
		}
	}

	quality = getCrystalQuality();
}

void LightsaberCrystalComponentImplementation::validateCrystalStats() {
	ManagedReference<LootManager*> lootManager = getZoneServer()->getLootManager();

	if (lootManager == nullptr)
		return;

	String crystalLoot = "lightsaber_module_force_crystal";

	if (isKraytPearl())
		crystalLoot = "lightsaber_module_krayt_dragon_pearl";

	const CrystalData* crystalData = lootManager->getCrystalData(crystalLoot);

	if (crystalData == nullptr) {
		error("Unable to find crystal stats for " + getObjectTemplate()->getTemplateFileName() + " in validateCrystalStats()");
		return;
	}

	int minStat = crystalData->getMinHitpoints();
	int maxStat = crystalData->getMaxHitpoints();

	if (getMaxCondition() > maxStat || getMaxCondition() < minStat)
		setMaxCondition(getRandomizedStat(minStat, maxStat, itemLevel));

	//removed by EiF team - EiF crystals work differently

	 if (isKraytPearl() || isPowerCrystal()) {
		minStat = crystalData->getMinDamage();
		maxStat = crystalData->getMaxDamage();

		if (damage != 0.0f)
			if (damage > maxStat || damage < minStat)
				damage = getRandomizedStat(minStat, maxStat, itemLevel);

		minStat = crystalData->getMinHealthSac();
		maxStat = crystalData->getMaxHealthSac();

		if (sacHealth != 0.0f)
			if (sacHealth > maxStat || sacHealth < minStat)
				sacHealth = getRandomizedStat(minStat, maxStat, itemLevel);

		minStat = crystalData->getMinActionSac();
		maxStat = crystalData->getMaxActionSac();

		if (sacAction != 0.0f)
			if (sacAction > maxStat || sacAction < minStat)
				sacAction = getRandomizedStat(minStat, maxStat, itemLevel);

		minStat = crystalData->getMinMindSac();
		maxStat = crystalData->getMaxMindSac();

		if (sacMind != 0.0f)
			if (sacMind > maxStat || sacMind < minStat)
				sacMind = getRandomizedStat(minStat, maxStat, itemLevel);

		minStat = crystalData->getMinWoundChance();
		maxStat = crystalData->getMaxWoundChance();

		if (woundChance != 0.0f)
			if (woundChance > maxStat || woundChance < minStat)
				woundChance = getRandomizedStat(minStat, maxStat, itemLevel);


		float minFloatStat = crystalData->getMinAttackSpeed();
		float maxFloatStat = crystalData->getMaxAttackSpeed();

		if (attackSpeed != 0.0f)
			if (attackSpeed > maxFloatStat || attackSpeed < minFloatStat)
				attackSpeed = Math::getPrecision(getRandomizedStat(minFloatStat, maxFloatStat, itemLevel), 2);
	} 
}

int LightsaberCrystalComponentImplementation::getCrystalQuality() {
	if (itemLevel < 40)
		return POOR;
	else if (itemLevel < 70)
		return FAIR;
	else if (itemLevel < 100)
		return GOOD;
	else if (itemLevel < 140)
		return QUALITY;
	else if (itemLevel < 220)
		return SELECT;
	else if (itemLevel < 330)
		return PREMIUM;
	else
		return FLAWLESS;
}

int LightsaberCrystalComponentImplementation::getRandomizedStat(int min, int max, int itemLevel) {
	bool invertedValues = false;
	int invertedMin = min;
	int invertedMax = max;

	if (min > max) {
		int temp = min;
		min = max;
		max = temp;

		invertedValues = true;
	}

	float avgLevel = (float)(itemLevel - 60) / 220.f;

	float midLevel = min + ((max - min) * avgLevel);

	if (midLevel < min) {
		max += (midLevel - min);
		midLevel = min;
	}

	if (midLevel > max) {
		min += (midLevel - max);
		midLevel = max;
	}

	int randMin = min + System::random((int)(midLevel + 0.5f) - min);
	int randMax = (int)(midLevel + 0.5f) + System::random(max - midLevel);

	int result = randMin + System::random(randMax - randMin);

	if (invertedValues)
		result = invertedMin + (invertedMax - result);

	return result;
}

float LightsaberCrystalComponentImplementation::getRandomizedStat(float min, float max, int itemLevel) {
	bool invertedValues = false;
	float invertedMin = min;
	float invertedMax = max;

	if (min > max) {
		float temp = min;
		min = max;
		max = temp;

		invertedValues = true;
	}

	float avgLevel = (float)(itemLevel - 60) / 220.f;

	float midLevel = min + ((max - min) * avgLevel);

	if (midLevel < min) {
		max += (midLevel - min);
		midLevel = min;
	}

	if (midLevel > max) {
		min += (midLevel - max);
		midLevel = max;
	}

	float randMin = System::getMTRand()->rand(midLevel - min) + min;
	float randMax = System::getMTRand()->rand(max - midLevel) + midLevel;

	float result = System::getMTRand()->rand(randMax - randMin) + randMin;

	if (invertedValues)
		result = invertedMin + (invertedMax - result);

	return result;
}

void LightsaberCrystalComponentImplementation::fillAttributeList(AttributeListMessage* alm, CreatureObject* object) {
	TangibleObjectImplementation::fillAttributeList(alm, object);


	PlayerObject* player = object->getPlayerObject();
	if (object->hasSkill("combat_jedi_novice") || player->isPrivileged()) {
		if (ownerID == 0) {
			StringBuffer str;
			str << "\\#pcontrast2 UNTUNED";
			alm->insertAttribute("crystal_owner", str);
		} else {
			alm->insertAttribute("crystal_owner", ownerName);
		}

		if (isFocusCrystal()) {
			StringBuffer str3;
			str3 << "@jedi_spam:saber_color_" << getColor();
			alm->insertAttribute("color", str3);
		} else {
			if (ownerID != 0 || player->isPrivileged()) {
				if (damage != 0) {
				StringBuffer txt;
				txt << Math::getPrecision(damage,1) << "%";
					alm->insertAttribute("mindamage", txt.toString());
					alm->insertAttribute("maxdamage", txt.toString());
				}
				if (attackSpeed != 0) {
					StringBuffer txt;
					txt << Math::getPrecision(attackSpeed,1) << "%";
					alm->insertAttribute("wpn_attack_speed", txt.toString());
				}
				if (woundChance != 0) {
					StringBuffer txt;
					txt << Math::getPrecision(woundChance,1) << "%";
					alm->insertAttribute("wpn_wound_chance", txt.toString());
				}
				if (sacHealth != 0) {
					StringBuffer txt;
					txt << Math::getPrecision(sacHealth,1) << "%";
					alm->insertAttribute("wpn_attack_cost_health", txt.toString());
				}
				if (sacAction != 0) {
					StringBuffer txt;
					txt << Math::getPrecision(sacAction,1) << "%";
					alm->insertAttribute("wpn_attack_cost_action", txt.toString());
				}
				if (sacMind != 0) {
					StringBuffer txt;
					txt << Math::getPrecision(sacMind,1) << "%";
					alm->insertAttribute("wpn_attack_cost_mind", txt.toString());
				}
				if (pointBlankAccuracy != 0) {
					StringBuffer txt;
					txt << Math::getPrecision(pointBlankAccuracy,1) << "%";
					alm->insertAttribute("zerorangemod", txt.toString());
				}
				if (idealAccuracy != 0) {
					StringBuffer txt;
					txt << Math::getPrecision(idealAccuracy,1) << "%";
					alm->insertAttribute("midrangemod", txt.toString());
				}
				// For debugging
				if (player->isPrivileged()) {
					StringBuffer str;
					str << "@jedi_spam:crystal_quality_" << getQuality();
					alm->insertAttribute("challenge_level", itemLevel);
					alm->insertAttribute("crystal_quality", str);
				}
			} else {
				StringBuffer str;
				str << "@jedi_spam:crystal_quality_" << getQuality();
				alm->insertAttribute("crystal_quality", str);
			}
		}
	}
}

void LightsaberCrystalComponentImplementation::fillObjectMenuResponse(ObjectMenuResponse* menuResponse, CreatureObject* player) {
	if (ownerID == 0 && player->hasSkill("combat_jedi_lightsaber_04") && hasPlayerAsParent(player) && player->isMeditating()) {
		String text = "@jedi_spam:tune_crystal";
		if (player->getForceAlignment() < -2500)
			text = "@jedi_spam:bleed_crystal";
		menuResponse->addRadialMenuItem(128, 3, text);
	}

	PlayerObject* ghost = player->getPlayerObject();
	if (ghost != nullptr && ghost->isPrivileged()) {
		menuResponse->addRadialMenuItem(129, 3, "Staff Commands");

		if (isKraytPearl() || isPowerCrystal())
			menuResponse->addRadialMenuItemToRadialID(129, 130, 3, "Recalculate Stats");

		if (ownerID != 0)
			menuResponse->addRadialMenuItemToRadialID(129, 131, 3, "Untune Crystal");
	}

	ComponentImplementation::fillObjectMenuResponse(menuResponse, player);
}

int LightsaberCrystalComponentImplementation::handleObjectMenuSelect(CreatureObject* player, byte selectedID) {
	if (selectedID == 128 && player->hasSkill("combat_jedi_lightsaber_04") && hasPlayerAsParent(player) && ownerID == 0 && player->isMeditating()) {
		ManagedReference<SuiMessageBox*> suiMessageBox = new SuiMessageBox(player, SuiWindowType::TUNE_CRYSTAL);

		suiMessageBox->setPromptTitle("@jedi_spam:confirm_tune_title");
		suiMessageBox->setPromptText("@jedi_spam:confirm_tune_prompt");
		suiMessageBox->setCancelButton(true, "Cancel");
		suiMessageBox->setUsingObject(_this.getReferenceUnsafeStaticCast());
		suiMessageBox->setCallback(new LightsaberCrystalTuneSuiCallback(player->getZoneServer()));

		player->getPlayerObject()->addSuiBox(suiMessageBox);
		player->sendMessage(suiMessageBox->generateMessage());
	}

	PlayerObject* ghost = player->getPlayerObject();
	if (ghost != nullptr && ghost->isPrivileged()){
		if (selectedID == 130 && (!isFocusCrystal())) {
			generateCrystalStats();
		} else if (selectedID == 131 && ownerID != 0) {
			ownerID = 0;

			String tuneName = StringIdManager::instance()->getStringId(objectName.getFullPath().hashCode()).toString();
			if (getCustomObjectName().toString().contains("(Exceptional)"))
				tuneName = tuneName + " (Exceptional)\\#.";
			else if (getCustomObjectName().toString().contains("(Legendary)"))
				tuneName = tuneName + " (Legendary)\\#.";
			else
				tuneName = tuneName + "\\#.";
		}
	}

	return 0;
}

bool LightsaberCrystalComponentImplementation::hasPlayerAsParent(CreatureObject* player) {
	ManagedReference<SceneObject*> wearableParent = getParentRecursively(SceneObjectType::WEARABLECONTAINER);
	SceneObject* inventory = player->getSlottedObject("inventory");
	SceneObject* bank = player->getSlottedObject("bank");

	// Check if crystal is inside a wearable container in bank or inventory
	if (wearableParent != nullptr) {
		ManagedReference<WearableContainerObject*> wearable = cast<WearableContainerObject*>(wearableParent.get());

		if (wearable != nullptr) {
			SceneObject* parentOfWearableParent = wearable->getParent().get();

			if (parentOfWearableParent == inventory || parentOfWearableParent == bank)
				return true;
		}
	} else {
		// Check if crystal is in inventory or bank
		SceneObject* thisParent = getParent().get();

		if (thisParent == inventory || thisParent == bank)
			return true;
	}
	return false;
}

void LightsaberCrystalComponentImplementation::tuneCrystal(CreatureObject* player) {
	if(!player->hasSkill("combat_jedi_lightsaber_04") || !hasPlayerAsParent(player) || !player->isMeditating()) {
		return;
	}

	if (isKraytPearl() || isPowerCrystal()) {
		ManagedReference<PlayerObject*> ghost = player->getPlayerObject();

		if (ghost == nullptr)
			return;

		int maxForce = ghost->getForcePowerMax();

		int tuningCost = maxForce * 0.75;

		if (ghost->getForcePower() <= tuningCost) {
			player->sendSystemMessage("@jedi_spam:no_force_power");
			return;
		}

		ghost->setForcePower(ghost->getForcePower() - tuningCost);
	}

	if (ownerID == 0) {
		validateCrystalStats();

		ownerID = player->getObjectID();
		ownerName = player->getDisplayedName();
		int colorIndex = getColor();

		// default Color code is lime green.
		String colorCode = "\\#00FF00";
		String tuneType = "(tuned)";

		if (player->getForceAlignment() < -2500) {
			colorCode = "\\#660000";
			tuneType = "(bled)";
			if (colorIndex < 128) {
				setColor(colorIndex + 128);
				updateCrystal(colorIndex + 128);
			}
		} else	{
			if (colorIndex >= 128) {
				setColor(colorIndex - 128);
				updateCrystal(colorIndex - 128);
			}
		}
			

		String tuneName = StringIdManager::instance()->getStringId(objectName.getFullPath().hashCode()).toString();
		if (getCustomObjectName().toString().contains("(Exceptional)"))
			tuneName = colorCode + tuneName + " (Exceptional) " + tuneType + "\\#.";
		else if (getCustomObjectName().toString().contains("(Legendary)"))
			tuneName = colorCode + tuneName + " (Legendary) " + tuneType + "\\#.";
		else
			tuneName = colorCode + tuneName + " " + tuneType + "\\#.";

		setCustomObjectName(tuneName, true);
		player->notifyObservers(ObserverEventType::TUNEDCRYSTAL, _this.getReferenceUnsafeStaticCast(), 0);
		player->sendSystemMessage("@jedi_spam:crystal_tune_success");
	}
}

void LightsaberCrystalComponentImplementation::updateCrystal(int value){
	byte type = 0x02;
	setCustomizationVariable(type, value, true);
}

void LightsaberCrystalComponentImplementation::updateCraftingValues(CraftingValues* values, bool firstUpdate) {
	int colorMax = values->getMaxValue("color");
	int color = values->getCurrentValue("color");

	if (isFocusCrystal()) {
		setColor(color);
		updateCrystal(color);
	} else {
		int randomColor = System::random(7) + 120;
		setColor(randomColor);
		updateCrystal(randomColor);
	}

	generateCrystalStats();

	ComponentImplementation::updateCraftingValues(values, firstUpdate);
}

int LightsaberCrystalComponentImplementation::inflictDamage(TangibleObject* attacker, int damageType, float damage, bool destroy, bool notifyClient) {
	if (isDestroyed()) {
		return 0;
	}

	TangibleObjectImplementation::inflictDamage(attacker, damageType, damage, destroy, notifyClient);

	if (isDestroyed()) {
		ManagedReference<WeaponObject*> weapon = cast<WeaponObject*>(_this.getReferenceUnsafeStaticCast()->getParent().get()->getParent().get().get());

		if (weapon != nullptr) {
			/*if (getColor() == 31) {
				weapon->setAttackSpeed(weapon->getAttackSpeed() - getAttackSpeed());
				weapon->setMinDamage(weapon->getMinDamage() - getDamage());
				weapon->setMaxDamage(weapon->getMaxDamage() - getDamage());
				weapon->setHealthAttackCost(weapon->getHealthAttackCost() - getSacHealth());
				weapon->setActionAttackCost(weapon->getActionAttackCost() - getSacAction());
				weapon->setMindAttackCost(weapon->getMindAttackCost() - getSacMind());
				weapon->setWoundsRatio(weapon->getWoundsRatio() - getWoundChance());
			}*/

			if (isFocusCrystal()) {
				weapon->setBladeColor(255);
				weapon->setCustomizationVariable("/private/index_color_blade", 255, true);

				if (weapon->isEquipped()) {
					ManagedReference<CreatureObject*> parent = cast<CreatureObject*>(weapon->getParent().get().get());
					ManagedReference<SceneObject*> inventory = parent->getSlottedObject("inventory");
					inventory->transferObject(weapon, -1, true, true);
					parent->sendSystemMessage("@jedi_spam:lightsaber_no_color"); //That lightsaber can not be used until it has a color-modifying Force crystal installed.
				}
			}
		}
	}

	return 0;
}

bool LightsaberCrystalComponentImplementation::isKraytPearl() {
	const String TemplateFileName =  getObjectTemplate()->getTemplateFileName();
	return TemplateFileName.contains("pearl");
}

bool LightsaberCrystalComponentImplementation::isPowerCrystal() {
	const String TemplateFileName =  getObjectTemplate()->getTemplateFileName();
	return TemplateFileName.contains("power");
}

bool LightsaberCrystalComponentImplementation::isFocusCrystal() {
	const String TemplateFileName =  getObjectTemplate()->getTemplateFileName();
	return TemplateFileName.contains("focus");
}
