/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.
*/

#include "SaberInventoryContainerComponent.h"
#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/tangible/weapon/WeaponObject.h"
#include "server/zone/objects/tangible/component/lightsaber/LightsaberCrystalComponent.h"

int SaberInventoryContainerComponent::canAddObject(SceneObject* sceneObject, SceneObject* object, int containmentType, String& errorDescription) const {

	ManagedReference<SceneObject*> p = sceneObject->getParent().get();

	if (p != nullptr){
		int containment = p->getContainmentType();

		if (containment == 4) {
			errorDescription = "@jedi_spam:saber_not_while_equpped";
			return TransferErrorCode::INVALIDTYPE;
		}
	}

	if (!object->isLightsaberCrystalObject()) {
		if (!isCore(object)) {
			errorDescription = "@jedi_spam:saber_not_crystal";
			return TransferErrorCode::INVALIDTYPE;
		} else {
			if (hasCore(sceneObject)) {
				errorDescription = "@jedi_spam:saber_already_has_core";
				return TransferErrorCode::INVALIDTYPE;
			}
			return 0;
		}

	}

	LightsaberCrystalComponent* crystal = cast<LightsaberCrystalComponent*> (object);

	if (crystal->getOwnerID() == 0) {
		errorDescription = "@jedi_spam:saber_crystal_not_tuned";
		return TransferErrorCode::INVALIDTYPE;
	}

	ManagedReference<CreatureObject*> creature = crystal->getParentRecursively(SceneObjectType::PLAYERCREATURE).castTo<CreatureObject*>();

	if (creature == nullptr || crystal->getOwnerID() != creature->getObjectID()){
		errorDescription = "@jedi_spam:saber_crystal_not_owner";
		return TransferErrorCode::INVALIDTYPE;
	}

	if (crystal->isDestroyed()) {
		errorDescription = "You cannot add a broken crystal to your lightsaber.";
		return TransferErrorCode::INVALIDTYPE;
	}

	if (sceneObject->getContainerObjectsSize() >= sceneObject->getContainerVolumeLimit()) {
		errorDescription = "@container_error_message:container03"; //This container is full.
		return TransferErrorCode::CONTAINERFULL;
	}

	if (crystal->isFocusCrystal() && hasFocusCrystal(sceneObject)) {
		errorDescription = "@jedi_spam:saber_already_has_color";
		return TransferErrorCode::INVALIDTYPE;
	} else if (crystal->isPowerCrystal() && hasPowerCrystal(sceneObject)) {
		errorDescription = "@jedi_spam:saber_already_has_power";
		return TransferErrorCode::INVALIDTYPE;
	} else if (crystal->isKraytPearl() && hasKraytPearl(sceneObject)) {
		errorDescription = "@jedi_spam:saber_already_has_pearl";
		return TransferErrorCode::INVALIDTYPE;
	}

	return 0;
}

/**
 * Is called when this object has been inserted with an object
 * @param object object that has been inserted
 */
int SaberInventoryContainerComponent::notifyObjectInserted(SceneObject* sceneObject, SceneObject* object) const {
	ManagedReference<WeaponObject*> weao = cast<WeaponObject*>( sceneObject->getParent().get().get());

	Locker locker(weao);

	if (weao->isJediWeapon()) {
		recalculateSaberStats(sceneObject);
		if (object->isLightsaberCrystalObject()) {
			ManagedReference<LightsaberCrystalComponent*> crystal = cast<LightsaberCrystalComponent*>( object);

			if (crystal->isFocusCrystal()) {
				int color = crystal->getColor();
				weao->setBladeColor(color);
				weao->setCustomizationVariable("/private/index_color_blade", color, true);
			}
		}
	}

	return sceneObject->notifyObjectInserted(object);
}

/**
 * Is called when an object was removed
 * @param object object that has been inserted
 */
int SaberInventoryContainerComponent::notifyObjectRemoved(SceneObject* sceneObject, SceneObject* object, SceneObject* destination) const {
	ManagedReference<WeaponObject*> weao = cast<WeaponObject*>( sceneObject->getParent().get().get());

	if (weao->isJediWeapon()) {
		recalculateSaberStats(sceneObject);
		if (object->isLightsaberCrystalObject()) {
			ManagedReference<LightsaberCrystalComponent*> crystal = cast<LightsaberCrystalComponent*>( object);

			if (crystal->isDestroyed()) {
				return sceneObject->notifyObjectRemoved(object);
			}

			Locker locker(weao);

			if (crystal->isFocusCrystal()) {
				weao->setBladeColor(255);
				weao->setCustomizationVariable("/private/index_color_blade", 255, true);
			}
		}
	}

	return sceneObject->notifyObjectRemoved(object);
}

bool SaberInventoryContainerComponent::checkContainerPermission(SceneObject* sceneObject, CreatureObject* creature, uint16 permission) const {
	ManagedReference<WeaponObject*> saber = cast<WeaponObject*>( sceneObject->getParent().get().get());

	if (saber == nullptr)
		return false;


	if (saber->isJediWeapon() && saber->isEquipped()) {
		ManagedReference<CreatureObject*> player = saber->getParentRecursively(SceneObjectType::PLAYERCREATURE).castTo<CreatureObject*>();

		if (player == nullptr)
			return false;

		player->sendSystemMessage("@jedi_spam:saber_not_while_equpped"); // You cannot modify the crystals in this lightsaber while it is equipped.

		return false;
	}

	return ContainerComponent::checkContainerPermission(sceneObject, creature, permission);
}

bool SaberInventoryContainerComponent::hasFocusCrystal(SceneObject* sceneObject) const {
	for (int i = 0; i < sceneObject->getContainerObjectsSize(); i++) {
		if (!sceneObject->getContainerObject(i)->isLightsaberCrystalObject())
			continue;
		Reference<LightsaberCrystalComponent*> crystalInside =  sceneObject->getContainerObject(i).castTo<LightsaberCrystalComponent*>();
		if (crystalInside->isFocusCrystal())
			return true;
	}
	return false;
}

bool SaberInventoryContainerComponent::hasPowerCrystal(SceneObject* sceneObject) const {
	for (int i = 0; i < sceneObject->getContainerObjectsSize(); i++) {
		if (!sceneObject->getContainerObject(i)->isLightsaberCrystalObject())
			continue;

		Reference<LightsaberCrystalComponent*> crystalInside =  sceneObject->getContainerObject(i).castTo<LightsaberCrystalComponent*>();
		if (crystalInside->isPowerCrystal())
			return true;
	}
	return false;
}

bool SaberInventoryContainerComponent::hasKraytPearl(SceneObject* sceneObject) const {
	for (int i = 0; i < sceneObject->getContainerObjectsSize(); i++) {
		if (!sceneObject->getContainerObject(i)->isLightsaberCrystalObject())
			continue;
		Reference<LightsaberCrystalComponent*> crystalInside =  sceneObject->getContainerObject(i).castTo<LightsaberCrystalComponent*>();
		if (crystalInside->isKraytPearl())
			return true;
	}
	return false;
}

bool SaberInventoryContainerComponent::hasCore(SceneObject* sceneObject) const {	
	for (int i = 0; i < sceneObject->getContainerObjectsSize(); i++) {
		if (!sceneObject->getContainerObject(i)->isLightsaberCrystalObject())
			return true;
	}
	return false;
}

bool SaberInventoryContainerComponent::isCore(SceneObject* sceneObject) const {	
	const String name =  sceneObject->getObjectNameStringIdName();
	return name.contains("lightsaber_core");
}


int SaberInventoryContainerComponent::recalculateSaberStats(SceneObject* sceneObject) const {
	resetSaberStats(sceneObject);

	ManagedReference<WeaponObject*> weao = cast<WeaponObject*>( sceneObject->getParent().get().get());

	if (weao->isJediWeapon()) {
		if (hasCore(sceneObject))
			applySaberCore(sceneObject);
		else
			return 0;

		if (hasPowerCrystal(sceneObject))
			applyPowerCrystal(sceneObject);

		if (hasKraytPearl(sceneObject))
			applyKraytPearl(sceneObject);
	}
	return 0;
}

int SaberInventoryContainerComponent::resetSaberStats(SceneObject* sceneObject) const {
	ManagedReference<WeaponObject*> weao = cast<WeaponObject*>( sceneObject->getParent().get().get());

	if (weao->isJediWeapon()) {
		weao->setPointBlankAccuracy(0);
		weao->setIdealAccuracy(0);
		weao->setMaxRangeAccuracy(0);
		weao->setMaxRange(5);
		weao->setIdealRange(0);
		weao->setAttackSpeed(8.0f);
		weao->setMinDamage(5.0f);
		weao->setMaxDamage(5.0f);
		weao->setWoundsRatio(12.0f);
		weao->setHealthAttackCost(5);
		weao->setActionAttackCost(5);
		weao->setMindAttackCost(5);
	}
	return 0;
}

int SaberInventoryContainerComponent::applySaberCore(SceneObject* sceneObject) const {
	ManagedReference<WeaponObject*> weao = cast<WeaponObject*>( sceneObject->getParent().get().get());

	for (int i = 0; i < sceneObject->getContainerObjectsSize(); i++) {
		SceneObject* scno = sceneObject->getContainerObject(i);
		if (!scno->isLightsaberCrystalObject()) {
			ManagedReference<Component*> core = cast<Component*>(scno);
			core->setMaxCondition(core->getAttributeValue("hitpoints"), true);
			weao->setPointBlankAccuracy(0 + core->getAttributeValue("zerorangemod"));
			weao->setIdealAccuracy(0 + core->getAttributeValue("midrangemod"));
			weao->setMaxRangeAccuracy(0 + core->getAttributeValue("maxrangemod"));
			weao->setIdealRange(0 + core->getAttributeValue("midrange"));
			weao->setAttackSpeed(8.0f + core->getAttributeValue("attackspeed"));
			weao->setMinDamage(5.0f + core->getAttributeValue("mindamage"));
			weao->setMaxDamage(5.0f + core->getAttributeValue("maxdamage"));
			weao->setWoundsRatio(12.0f + core->getAttributeValue("woundchance"));
			weao->setHealthAttackCost(5 + core->getAttributeValue("attackhealthcost"));
			weao->setActionAttackCost(5 + core->getAttributeValue("attackactioncost"));
			weao->setMindAttackCost(5 + core->getAttributeValue("attackmindcost"));
		}
	}
	return 0;
}

int SaberInventoryContainerComponent::applyPowerCrystal(SceneObject* sceneObject) const {
	ManagedReference<WeaponObject*> weao = cast<WeaponObject*>( sceneObject->getParent().get().get());

	for (int i = 0; i < sceneObject->getContainerObjectsSize(); i++) {
		if (!sceneObject->getContainerObject(i)->isLightsaberCrystalObject())
			continue;
		Reference<LightsaberCrystalComponent*> crystalInside =  sceneObject->getContainerObject(i).castTo<LightsaberCrystalComponent*>();
		if (crystalInside->isPowerCrystal()) {
			float crystalDamage = crystalInside->getDamage();
			float crystalSpeed = crystalInside->getAttackSpeed();
			float crystalAction = crystalInside->getSacAction();
			float crystalMind = crystalInside->getSacMind();
			float crystalWoundChance = crystalInside->getWoundChance();
			float crystalIdealAccuracy = crystalInside->getIdealAccuracy();
			float crystalPointBlankAccuracy = crystalInside->getPointBlankAccuracy();
			if (crystalDamage != 0.0f) {
				crystalDamage = (crystalDamage + 100) / 100;
				weao->setMinDamage(weao->getMinDamage() * crystalDamage);
				weao->setMaxDamage(weao->getMaxDamage() * crystalDamage);
			}
			if (crystalSpeed != 0.0f) {
				crystalSpeed = (100 - crystalSpeed) / 100;
				weao->setAttackSpeed(weao->getAttackSpeed() * crystalSpeed);
			}
			if (crystalAction != 0.0f) {
				crystalAction = (100 - crystalAction) / 100;
				weao->setActionAttackCost(weao->getActionAttackCost() * crystalAction);
			}
			if (crystalMind != 0.0f) {
				crystalMind = (100 - crystalMind) / 100;
				weao->setMindAttackCost(weao->getMindAttackCost() * crystalMind);
			}
			if (crystalWoundChance != 0.0f) {
				crystalWoundChance = (100 + crystalWoundChance) / 100;
				weao->setWoundsRatio(weao->getWoundsRatio() * crystalWoundChance);
			}
			if (crystalIdealAccuracy != 0.0f) {
				crystalIdealAccuracy = (100 + crystalIdealAccuracy) / 100;
				weao->setIdealAccuracy(weao->getIdealAccuracy() * crystalIdealAccuracy);
			}
			if (crystalPointBlankAccuracy != 0.0f) {
				crystalPointBlankAccuracy = (100 + crystalPointBlankAccuracy) / 100;
				weao->setPointBlankAccuracy(weao->getPointBlankAccuracy() * crystalPointBlankAccuracy);
			}
		}
	}
	return 0;
}

int SaberInventoryContainerComponent::applyKraytPearl(SceneObject* sceneObject) const {
	ManagedReference<WeaponObject*> weao = cast<WeaponObject*>( sceneObject->getParent().get().get());

	for (int i = 0; i < sceneObject->getContainerObjectsSize(); i++) {
		if (!sceneObject->getContainerObject(i)->isLightsaberCrystalObject())
			continue;
		Reference<LightsaberCrystalComponent*> crystalInside =  sceneObject->getContainerObject(i).castTo<LightsaberCrystalComponent*>();
		if (crystalInside->isKraytPearl()) {
			float pearlDamage = crystalInside->getDamage();
			float pearlSpeed = crystalInside->getAttackSpeed();
			if (pearlDamage != 0.0f) {
				pearlDamage = (pearlDamage + 100) / 100;
				weao->setMinDamage(weao->getMinDamage() * pearlDamage);
				weao->setMaxDamage(weao->getMaxDamage() * pearlDamage);
			}
			if (pearlSpeed != 0.0f) {
				pearlSpeed = (100 - pearlSpeed) / 100;
				weao->setAttackSpeed(weao->getAttackSpeed() * pearlSpeed);
			}
		}
	}
	return 0;
}
