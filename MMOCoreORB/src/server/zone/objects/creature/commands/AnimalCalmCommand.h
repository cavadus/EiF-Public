/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef ANIMALCALMCOMMAND_H_
#define ANIMALCALMCOMMAND_H_

#include "server/zone/objects/creature/commands/JediQueueCommand.h"
#include "ForcePowersQueueCommand.h"
#include "server/zone/objects/creature/ai/Creature.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "server/zone/objects/tangible/threat/ThreatMap.h"

class AnimalCalmCommand : public ForcePowersQueueCommand {
public:

	AnimalCalmCommand(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* jedi, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(jedi))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(jedi))
			return INVALIDLOCOMOTION;


		ManagedReference<SceneObject*> targetObject = server->getZoneServer()->getObject(target);

		Creature* targetCreature = cast<Creature*>(targetObject.get());

		if (targetCreature == nullptr ) {
			return INVALIDTARGET;
		}

		if (!targetCreature->isCreature()) {
			jedi->sendSystemMessage("@error_message:target_not_creature");
			return true;
		}

		if (targetCreature->getDistanceTo(jedi) > 32.f) {
     			jedi->sendSystemMessage("@error_message:target_out_of_range");
			return true;
   	}


		// Research in the Mantis 5247 suggests command only worked on CASTER's actual primary enemy creature,
			// probably to prevent  griefing , so players couldn't go around 'calming' other people's spawns.
			if (targetCreature->getMainDefender() != jedi) {
				jedi->sendSystemMessage("@error_message:not_your_target"); //You cant attack that target...
				return true;
			}

		int jediPower = jedi->getSkillMod("jedi_force_power_max")/25.0;
		int powerDifference = jediPower - targetCreature->getLevel(); 
		if (powerDifference > 95 )
			powerDifference = 95;

		int roll = System::random(100);
		if ( roll > powerDifference){
			jedi->sendSystemMessage("The creature's mind is too strong for your powers!"); 
			return true;
		} else {

			// Commence calming action...

			Locker clocker(targetCreature, jedi);

			targetCreature->removeDefender(jedi);
			targetCreature->notifyObservers(ObserverEventType::DEFENDERDROPPED);
			targetCreature->getThreatMap()->clearAggro(jedi);

			jedi->doCombatAnimation(targetCreature, STRING_HASHCODE("mind_trick_1"), 1, 0);
			jedi->sendSystemMessage("@jedi_spam:calm_target"); //"You successfully calm down your target."

			return SUCCESS;

		}

		return SUCCESS;
	}
};

#endif //ANIMALCALMCOMMAND_H_
