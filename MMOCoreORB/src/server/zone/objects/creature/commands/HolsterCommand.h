/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef HOLSTERCOMMAND_H_
#define HOLSTERCOMMAND_H_

class HolsterCommand : public QueueCommand {
public:

	HolsterCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->isInCombat()) {
			creature->sendSystemMessage("You cannot holster a weapon in combat!");
			return GENERALERROR;
		}

		ManagedReference<WeaponObject*> heldWeapon = creature->getSlottedObject("hold_r").castTo<WeaponObject*>();
		ManagedReference<WeaponObject*> holsteredWeapon = creature->getHolsteredWeapon();
		if (holsteredWeapon != nullptr)
			holsteredWeapon->unholsterWeapon(creature);
		else if (heldWeapon != nullptr)
			heldWeapon->holsterWeapon(creature);

		// creature->info("transfer item command");
		return SUCCESS;
	}

};

#endif //HOLSTERCOMMAND_H_
