/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef STOPPINGSHOTCOMMAND_H_
#define STOPPINGSHOTCOMMAND_H_

#include "CombatQueueCommand.h"

class StoppingShotCommand : public CombatQueueCommand {
public:

	StoppingShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		ManagedReference<SceneObject*> targetObject = server->getZoneServer()->getObject(target);
	
		// Add Damage if Closer
 		if ( targetObject != nullptr && targetObject->isInRange(creature, 15.0)) {
			UnicodeString args = "healthDamageMultiplier=2.0f";
			return doCombatAction(creature, target, args);
		 } else if ( targetObject != nullptr && targetObject->isInRange(creature, 32.0)) {
			UnicodeString args = "healthDamageMultiplier=1.75f";
			return doCombatAction(creature, target, args);
		 } else {
			return doCombatAction(creature, target);
		 }
	}

};

#endif //STOPPINGSHOTCOMMAND_H_
