/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCESHIELD1COMMAND_H_
#define FORCESHIELD1COMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "QueueCommand.h"

class ForceShield1Command : public JediQueueCommand {
public:

	ForceShield1Command(const String& name, ZoneProcessServer* server) : JediQueueCommand(name, server) {
		buffCRC = BuffCRC::JEDI_FORCE_SHIELD_1;
		blockingCRCs.add(BuffCRC::JEDI_FORCE_SHIELD_2);
		blockingCRCs.add(BuffCRC::JEDI_FORCE_ABSORB_1);
		blockingCRCs.add(BuffCRC::JEDI_AVOID_INCAPACITATION);
		blockingCRCs.add(BuffCRC::JEDI_FORCE_FEEDBACK_1);
		singleUseEventTypes.add(ObserverEventType::FORCESHIELD);
		skillMods.put("force_shield", 2500);
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const override {
		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->checkCooldownRecovery("JEDI_FORCE_SHIELD_1")) {
			creature->sendSystemMessage("You are still recovering and cannot create a shield right now."); //You cannot burst run right now.
			return false;
		} else if( creature->isKnockedDown()){
			creature->sendSystemMessage("You cannot currently create a shield.");
			return false;
		} else {
			int res =  doJediSelfBuffCommand(creature);
			if (res != SUCCESS) {
				return res;
			}
			int forceAlignment = creature->getForceAlignment();
			forceAlignment += 2;
			creature->setForceAlignment(forceAlignment);
			creature->updateCooldownTimer("JEDI_FORCE_SHIELD_1", (duration * 1000)+20000);
			return SUCCESS;
		}
	}

	void handleBuff(SceneObject* creature, ManagedObject* object, int64 param) const override {
		ManagedReference<CreatureObject*> player = creature->asCreatureObject();

		if (player == nullptr)
			return;

		ManagedReference<PlayerObject*> ghost = player->getPlayerObject();

		if (ghost == nullptr)
			return;

		// Client Effect upon hit (needed)
		player->playEffect("clienteffect/pl_force_shield_hit.cef", "");
		int forceAlignment = player->getForceAlignment();
		float forceAlignmentMod = 10000.0/forceAlignment;

		int damage = param;
		Buff* buff = player->getBuff(BuffCRC::JEDI_FORCE_SHIELD_1);
		int shieldRemaining = buff -> getSkillModifierValue("force_shield");
		// DEBUG Message uncomment.
		// player->sendSystemMessage("Shield Event: " + String::valueOf(shieldRemaining) + " remaining, damage: " + String::valueOf(damage)); 
		if ( shieldRemaining <= damage) { // Remove buff if no shield remaining.
			if (buff != nullptr) {
				Locker locker(buff);
				buff -> setSkillModifier("force_shield", 0);
				player->removeBuff(BuffCRC::JEDI_FORCE_SHIELD_1);
			}
		} else {
			if ( buff != nullptr) {
				Locker locker(buff);
				int updatedShieldRemaining = shieldRemaining -  damage;
				buff -> setSkillModifier("force_shield", updatedShieldRemaining);
			}
			
		}
	}

};

#endif //FORCESHIELD1COMMAND_H_
