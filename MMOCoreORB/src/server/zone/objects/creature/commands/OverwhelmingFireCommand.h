/*EiF 2020
Please do not use without permission*/

#ifndef OVERWHELMINGFIRECOMMAND_H_
#define OVERWHELMINGFIRECOMMAND_H_

#include "CombatQueueCommand.h"

class OverwhelmingFireCommand : public CombatQueueCommand {
public:

	OverwhelmingFireCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //OVERWHELMINGFIRECOMMAND_H_
