/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef ANIMALATTACKCOMMAND_H_
#define ANIMALATTACKCOMMAND_H_

#include "ForcePowersQueueCommand.h"
#include "server/zone/objects/creature/ai/Creature.h"
#include "server/zone/managers/combat/CombatManager.h"

class AnimalAttackCommand : public ForcePowersQueueCommand {
public:

	AnimalAttackCommand(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* jedi, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(jedi))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(jedi))
			return INVALIDLOCOMOTION;


		ManagedReference<SceneObject*> targetObject = server->getZoneServer()->getObject(target);

		Creature* targetCreature = cast<Creature*>(targetObject.get());

		if (targetCreature == nullptr )
			return INVALIDTARGET;


		if (targetCreature->getDistanceTo(jedi) > 48.f) {
			 jedi->sendSystemMessage("@error_message:target_out_of_range");
			 return true;
		}


		TangibleObject* defender = cast<TangibleObject*>(jedi->getMainDefender());

		if (defender == nullptr ) {
			return INVALIDTARGET;
		}

		if (defender == targetCreature) {
			jedi->sendSystemMessage("A creature cannot attack itself!");
			return true;
		}

		if (!defender->isAttackableBy(targetCreature)) {
			jedi->sendSystemMessage("This creature will not aid you against your foe!");
			return INVALIDTARGET;
		}


		int jediPower = jedi->getSkillMod("jedi_force_power_max")/25.0;
		int powerDifference = jediPower - targetCreature->getLevel(); 
		if (powerDifference > 95 ){
			powerDifference = 95;
		}
		int roll =  System::random(100);
		if ( roll > powerDifference){
			jedi->sendSystemMessage("The creature's mind is too strong for your powers!"); 
			return true;
		} else {

			Locker clocker(targetCreature, jedi);

			CombatManager::instance()->forcePeace(targetCreature);

			targetCreature->setDefender(defender);

			jedi->doCombatAnimation(targetCreature, STRING_HASHCODE("mind_trick_1"), 1, 0);
			jedi->sendSystemMessage("The creature comes to your aid!"); //"You successfully enrage your target."

			CombatManager::instance()->broadcastCombatSpam(jedi, targetCreature, NULL, 0, "cbt_spam", combatSpam + "_hit", 1);

			return SUCCESS;
		}

		return SUCCESS;
	}
};

#endif //ANIMALATTACKCOMMAND_H_
